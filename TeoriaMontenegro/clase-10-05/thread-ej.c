// Programa que crea 2 threads que imprimen a stdout del proceso 

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>	// headers de POSIX Threads

//declaracion de funcion para el thread
void *thread_imprime( void *ptr );

int main(void)
{
	pthread_t bugs, lucas;		//para guardar identificadores de threads
	const char *mensaje1 = "Temporada de pato!";
	const char *mensaje2 = "No! temporada de conejo!";
	int  iret1, iret2;
	char id[10]="MAIN";	//para imprimir el thread ID
	// creacion de threads independientes

	iret1 = pthread_create( &bugs, NULL, thread_imprime, (void*) mensaje1);
	if(iret1)
	{
		fprintf(stderr,"%s: Error - pthread_create() return code: %d\n",id, iret1);
		exit(EXIT_FAILURE);
	}

	sleep(2);	// lo aguantamos un toque para ver los mensajes bien

	iret2 = pthread_create( &lucas, NULL, thread_imprime, (void*) mensaje2);
	if(iret2)
	{
		fprintf(stderr,"%s: Error - pthread_create() return code: %d\n",id, iret2);
		exit(EXIT_FAILURE);
	}

	printf("%s: pthread_create() para thread 'Bugs' retorna: %d\n",id,iret1);
	printf("%s: pthread_create() para thread 'Lucas' retorna: %d\n",id,iret2);

	printf("%s: aguardando que terminen de pelear Bugs y Lucas...\n ", id);

	/* 
	* espera a que terminen todos los threads antes de continuar el 
	* programa principal, sino se corre el riesgo de terminal el main()
	* antes que termine algun thread, esto lo cortaria abruptamente
	* ya que al morir el proceso que contiene los threads mueren todos
	* ellos
	*/

	pthread_join( bugs, NULL);
	pthread_join( lucas, NULL); 

	printf("%s: ahora si, terminando ejecucion.\n ", id);

	exit(EXIT_SUCCESS);
}

void *thread_imprime( void *ptr )
{
	
    char *mens;
    char id[10];	//para imprimir el thread ID
    sprintf(id, "%d", (unsigned int)pthread_self());
    
    
    mens = (char *) ptr;		//lo casteamos
    
    
    printf("%s: %s \n", id, mens);
    sleep(2);	// para ver que terminan a tiempos diferentes
}
