#include<stdio.h>
#include<string.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<sys/types.h>
#include<sys/socket.h>

#define PORTtcp 8145



int main (void)
 {
  int fdSocket, fdtSocket,clntLen;
  struct sockaddr_in ServerTcp, ClienteTcp;
  socklen_t tamSocket;
  char buff[50];
 
  fdSocket=socket(AF_INET,SOCK_STREAM,0);
  memset(&ServerTcp,0,sizeof(ServerTcp));
  ServerTcp.sin_family=AF_INET;
  ServerTcp.sin_port=htons(PORTtcp);
  ServerTcp.sin_addr.s_addr=htonl(INADDR_ANY);

  bind(fdSocket,(struct sockaddr*)&ServerTcp,sizeof(ServerTcp));

  listen(fdSocket,5);
  clntLen=sizeof(ClienteTcp);
  while (1)
  {
   printf("\nEsperando Conexiones\n");
   fdtSocket=accept(fdSocket,(struct sockaddr*)&ClienteTcp,&clntLen);
   while(1)
   { 
    if(read(fdtSocket,buff,50)==0)
     {
      printf("\nConexion Finalizada\n");
      close (fdtSocket);
      break;
     }
    printf("\nMensaje Recibido %s\n",buff);
   } 
  }

}

