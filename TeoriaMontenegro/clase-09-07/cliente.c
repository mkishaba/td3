#include<stdio.h>
#include<string.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<sys/types.h>
#include<sys/socket.h>

#define PORTTCP 8145
#define PORTCLI 4555

int main(int cant, void *param[])
 {
  int fdSocket;
  struct sockaddr_in ServerTcp,ClienteTcp;
  char buff[50];


  fdSocket=socket(AF_INET,SOCK_STREAM,0);
  memset(&ClienteTcp,0,sizeof(ClienteTcp));
  memset(&ServerTcp,0,sizeof(ServerTcp));
  
  ServerTcp.sin_family=AF_INET;
  ServerTcp.sin_port=htons(PORTTCP);
  ServerTcp.sin_addr.s_addr=inet_addr(param[1]);
  
  ClienteTcp.sin_family=AF_INET;
  ClienteTcp.sin_port=htons(PORTCLI);
  ClienteTcp.sin_addr.s_addr=htonl(INADDR_ANY);
  
  bind(fdSocket,(struct sockaddr*)&ClienteTcp,sizeof(ClienteTcp));
  
  connect(fdSocket,(struct sockaddr*)&ServerTcp,sizeof(ServerTcp));
  
  while(strncmp(buff,"FIN",3))
  {
   printf("\nEscriba su mensaje: ");
   gets(buff);
   send(fdSocket,buff,strlen(buff)+1,0);
   }
  close(fdSocket);
}

