#include <stdio.h>
#include <unistd.h>
#include <signal.h>

void mi_handler (int s){
	
	printf ("\nHANDLER: llego la SIGUSR1\n");
	
	}
	
int main(){
	pid_t p;
	
	signal(SIGUSR1, mi_handler);
	
	printf("mi PID es %d\n",getpid());
	printf("ingrese nro de PID destino:");
	scanf("%d",&p);
	
	printf("enviando SIGUSR2 a proceso nro %d ...\n",p);
	sleep(1);
	kill(p, SIGUSR2);
	while (1) sleep(1);
	
	return 0;
}
