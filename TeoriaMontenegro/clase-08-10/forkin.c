#include <stdio.h>
#include <unistd.h>

int main(){
	pid_t p;
	int a=1;
	
	printf("la variable a vale: %d\n",a);
	p = fork();
	if (p) {
		sleep(1);
		printf("PADRE: mi variable a vale: %d\n",a);
		sleep(8);
		printf("PADRE: mi variable a vale: %d\n",a);
		sleep(8);
		printf("PADRE: finalizando ejecucion.....\n");
	}else{
		printf("HIJO: mi variable a antes de modificar vale: %d\n",a);
		a=0;
		sleep(5);
		printf("HIJO: mi variable a ahora vale: %d\n",a);
		sleep(1);
		printf("HIJO: finalizando ejecucion.....\n");
	}
	return 0;
}
