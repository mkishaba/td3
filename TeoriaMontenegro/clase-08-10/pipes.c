#include <stdio.h>
#include <unistd.h>
#include <signal.h>


int main(){



	int fds[2];
	pid_t p;
	char buffer[256];	
	
	pipe (fds);
	
	p = fork();
	if (p) {
		//proceso padre escribe en el pipe
		printf("PADRE: escribiendo HOLA en el pipe...\n");
		sleep(3);
		write (fds[1],"HOLA",5);
	}else{
		//proceso hijo escribe en el pipe

		
		printf("HIJO: leyendo pipe....\n");
		read (fds[0],buffer,5);
		printf("HIJO: recibido por el pipe: '%s'...\n",buffer);
	}
	while (1) ;
		
}
