#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

/*
	Ejemplo de uso de FIFOs
	debe estar creado previamente con el nombre mififo.pipe
	con el comando "mknod mififo.pipe p"
	luego se crea un proceso hijo que intercambia mensajes
	usando el pipe con el proceso padre.
	* 
	* tarea para el alumno: que pasa si uno de los dos
	* cierra el pipe antes que el otro lo pueda leer?

*/



int main(){

	int fdfifo,r;
	pid_t p;
	char buffer[256];
		
	
	p = fork();
	if (p) {
		fdfifo = open("mififo.pipe",O_RDWR);	//lo abro 
		//proceso padre escribe en el pipe
		printf("PADRE: mi PID es %d\n",getpid());
		sleep(1);
		printf("PADRE: escribiendo 'HOLA' en el fifopipe...\n");
		sleep(3);
		r = write (fdfifo,"HOLA",5);
		
		if (r == -1) {
			perror("PADRE: error leyendo FIFO:");
		}else{
			sleep(1);
			printf("PADRE: esperando respuesta del hijo...\n");
			sleep(3);
			read (fdfifo,buffer,9);
			printf("PADRE: recibido por el fifopipe: '%s'...\n",buffer);
			sleep(1);
			
		}
		printf("PADRE: cerrando pipe...\n");

		close (fdfifo);
		
	}else{
		fdfifo = open("mififo.pipe",O_RDWR);	//lo abro
		//proceso hijo lee del pipe
		printf("HIJO: mi PID es %d\n",getpid());
		sleep(1);
		
		printf("HIJO: leyendo fifopipe....\n");
		read (fdfifo,buffer,5);
		printf("HIJO: recibido por el fifopipe: '%s'...\n",buffer);
		sleep(1);
		printf("HIJO: contestando 'QUE TAL?' por el fifopipe....\n");
		write (fdfifo,"QUE TAL?",9);
		sleep(2);
		printf("HIJO: cerrando pipe...\n"); 
		close (fdfifo);
	
	}
	printf("PID: %d terminando ejecucion...\n",getpid());
	sleep(1);
	return 0;
	
		
}
