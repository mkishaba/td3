#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>

struct msgbuf {
                long mtype;      
                char mtext[200]  
            };


int main (void)
{
 char buff[200];
 int msgid;
 key_t clave;
 struct msgbuf msg;


 clave=ftok(".", 101);
 msgid=msgget(clave,IPC_CREAT | 0660);

 while(strncmp(buff,"FIN",3))
 {
  printf("\nIndique Canal a extraer (mtype): ");
  gets(buff);
  msgrcv(msgid,&msg,sizeof(msg),atol(buff),0);
  printf("Mensaje Recibido: %s\n",msg.mtext);
  
 }

 return(0);
}
  

 
