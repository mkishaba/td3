#include <stdio.h>
#include <math.h>
#include "i2cfunc.h"

// we will use I2C2 which is enumerated as 1 on the BBB
#define I2CBUS 1

typedef struct bmp085_coeff_s
{
  short int ac1;
  short int ac2;
  short int ac3;
  unsigned short int ac4;
  unsigned short int ac5;
  unsigned short int ac6;
  short int b1;
  short int b2;
  short int mb;
  short int mc;
  short int md;
} bmp085_coeff_t;

const unsigned int conversion_delay[4]={6, 10, 15, 30};

int main(int argc, char *argv[])
{
  int i;
  int handle;
  unsigned char tval;
  bmp085_coeff_t coeff;
  unsigned char *cbuf;
  unsigned char buf[10];
  unsigned char oss;
  long ut;
  long up;
  long x1, x2, x3;
  long b3, b4, b5, b6, b7;
  long p;
  long t;
  float deg;
  float kpa;
  float alt;
  float pe0 = 1010.5;


   if( argc == 2 )
   {
	pe0 = atol(argv[1]);
   }else{
      	printf("\n Uso: %s <presion> \n\n\t <presion> : presion a 0 msnm en hPa\n\n", argv[0]);
	return 0;
   }


  oss=3; // pressure conversion mode: 3=ultra-high-res, down to 0 which is ultra-low-power
  cbuf=(unsigned char *)&coeff;
  handle=i2c_open(I2CBUS, 0x77);

  // read BMP085 coefficients (22 bytes)
  i2c_write_byte(handle, 0xaa);
  i2c_read(handle, cbuf, 22);

  // swap the order of bytes due to endianness
  for (i=0; i<22; i=i+2)
  {
    tval=cbuf[i];
    cbuf[i]=cbuf[i+1];
    cbuf[i+1]=tval;
  }


  // read uncompensated temperature register
  buf[0]=0xf4;
  buf[1]=0x2e;
  i2c_write(handle, buf, 2);
  delay_ms(6);
  i2c_write_byte(handle, 0xf6);
  i2c_read(handle, buf, 2);
  ut=(((long)buf[0])<<8) | (long)buf[1];

  
  buf[0]=0xf4;
  buf[1]=0x34+(oss<<6);
  i2c_write(handle, buf, 2);
  delay_ms(conversion_delay[oss]);
  i2c_write_byte(handle, 0xf6);
  i2c_read(handle, buf, 3);
  up=(((long)buf[0])<<16) | (((long)buf[1])<<8) | (long)buf[2];
  up=up>>(8-oss);

  // we are done with the I2C
  i2c_close(handle);

  // now do the calculations for temperature
  x1=(ut-(long)coeff.ac6)*((long)coeff.ac5)/32768;
  x2=((long)coeff.mc)*2048/(x1+(long)coeff.md);
  b5=x1+x2;
  t=(b5+8)/16;
  deg=((float)t)/10;



  // calculations for the pressure
  b6=b5-4000;
  x1=(((long)coeff.b2)*(b6*b6/4096))/2048;
  x2=((long)coeff.ac2)*b6/2048;
  x3=x1+x2;
  
  b3=(((((long)coeff.ac1)*4+x3)<<oss) +2)/4;
  x1=(((long)coeff.ac3)*b6)>>13;
  x2=(((long)coeff.b1)*(b6*b6>>12))>>16;
  x3=(x1+x2+2)>>2;
  b4=((long)coeff.ac4)*(unsigned long)(x3+32768)/32768;
  b7=((unsigned long)up - b3)*(50000>>oss);
  if (b7<0x80000000)
    p=(((unsigned long)b7)<<1)/b4;
  else
    p=(((unsigned long)b7)/b4)*2;

  x1=(p/256);
  x1=x1*x1;  
  
  x1=(x1*3038)>>16;
  x2=(-7357*p)>>16;
  p+=(x1+x2+3791)>>4;
  kpa=((float)p)/100;

  alt= (deg+273.15)*29.27*logf(pe0/kpa);

// print out the results
  printf("Temp.   %.1f C\n", deg);
  printf("Presion %.3f hPa\n", kpa);
  printf("Altitud %.3f msnm\n", alt);
  return(0);
}

