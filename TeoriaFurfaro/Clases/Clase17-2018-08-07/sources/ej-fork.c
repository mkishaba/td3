/**
 * \file ej-fork.c
 * \brief Ejemplo simple de uso de fork
 * \details 
 * Este ejemplo sencillo muestra el uso y funcionamiento de la primitiva fork (). Se crea desde el proceso principal una instancia child (hijo), la cual al igual que el padre ejecuta una demora de 180 segundo para desde otra terminal poderlos visualizar con htop o con ps -elf | grep ej-fork, y estudiar sus PIDs, PPIDs, y su estado (observar columna S en htop).
 * Una vez ejecutado comentar la línea anteúltima del programa que pone a dormir al padre de modo que éste proceso termine de inmediato. Repetir la observación en otra terminal con htop o ps y observar el estado del proceso hijo.
 * Finalmente descomentar la línea del proceso padre que contiene la función wait. Repetir la observación y ver el estado del padre mientras el hijo no termina. ¿Es wait () una función bloqueante?
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

// cont = 0;

int main (void) 
{
  int pid; 
  pid = fork();                                // Creo un proceso hijo
 
  // Hijo
  if (!pid) 
  {
    printf ("Hijo. PID: %d\n",getpid());
    printf ("Hijo: Mi variable pid vale %d\n", pid);
//    sleep (60);
    exit (0);
  }
 
  // Padre
  printf ("Padre. PID: %d. PID hijo: %d\n",getpid(), pid);
  printf ("Padre: Mi variable pid vale %d\n", pid);
  sleep (1);
  sleep (30);
  wait (NULL);
  sleep (30);
  return 0;
}

