/** 
 * \file pipes.c
 * \brief Ejemplo para mostrar el uso de pipes uniendo dos comandos
 * \details Utilizando fork se genera una instancia child. Ambas tienen sus respectivos file descriptors para acceder a un pipe creado previamente a ejecutar fork () (herencia de variables por parte del child).
 * Empleando dup se copian los files descriptors de lectura del pipe en el stdin del child y el de escritura del pipe en el stdout del padre. Esto es equivalente a la redirección.
 * Mediante la primitiva exceclp se reemplazan los códigos de los procesos padre e hijo por los ejecutables 'ps -elf' y 'grep' respectivamente.
 * El resultado es el mismo que ejecutar en el mismo directorio la orden ps -elf| grep "proceso"
 * EL nombre proceso se pasa como argumento por línea de comando
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    int pfds[2];
    if (argc!=2)
    {
        fprintf(stderr,"Cantidad de argumentos insuficientes.\n");
        fprintf(stderr,"Uso %s NombreDelProceso.\n",argv[06]);
        exit (1);
    }
    pipe(pfds);
    if (!fork()) {
        close(1);       /* close normal stdout */
        dup(pfds[1]);   /* make stdout same as pfds[1] */
        close(pfds[0]); /* we don't need this */
        execlp("ps", "ps", "-elf", NULL);
    } else {
        close(0);       /* close normal stdin */
        dup(pfds[0]);   /* make stdin same as pfds[0] */
        close(pfds[1]); /* we don't need this */
        execlp("grep", "grep", argv[1], NULL);
    }
}
