#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define SHM_SIZE 20000

int main(int argc, char *argv[])
{
  key_t key;
  int shmid;
  char *data;
//   int mode;

  if (argc > 2) {
     fprintf(stderr, "Uso: shmdemo [datos_a_escribir]\n");
     exit(1);
  }

  if ((key = ftok("shmdemo.c", 'R')) == -1) 
  {
     perror("ftok");
     exit(1);
  }

  if ((shmid = shmget(key, SHM_SIZE, 0644 | IPC_CREAT)) == -1) 
  {
      perror("shmget");
      exit(1);
   }

  data = shmat(shmid, (void *)0, 0);
  if (data == (char *)(-1)) 
  {
    perror("shmat");
    exit(1);
  }

  if (argc == 2) 
  {
    printf("Escribiendo en el segmento de memoria: \"%s\"\n", argv[1]);
    strncpy(data, argv[1], SHM_SIZE);
  } 
  else
     printf("El contendio del segmento de memoria es: \"%s\"\n", data);

  if (shmdt(data) == -1) 
  {
      perror("shmdt");
      exit(1);
  }

  return 0;
 }
