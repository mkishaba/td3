#include <linux/init.h>   // Macros para module development. (__init , y __exit)
#include <linux/module.h> // Header general para module development.
#include <linux/kernel.h> // Macros y funciones del kernel.

// Estas Macros proveen información del modulo cuando se ejecuta desde shell el comando modinfo.
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alejandro Furfaro");
MODULE_VERSION("1.0");
MODULE_DESCRIPTION("Helo World LKM");


//Función de Inicio del modulo. (Init function)
static int __init helloLKM_init(void){
   printk(KERN_INFO "Modulo init: Hello!! FIRST_LKM\n");
   return 0;
}

//Función de salida del module. (Clean up function).
static void __exit helloLKM_exit(void){
   printk(KERN_INFO "Exit Module: Me removés??. Ya no se respeta ni al kernel!!. FIRST_LKM\n");
}

//Macros para  indicarle al kernel cuales son las funciones de inicialización y finalización.
module_init(helloLKM_init);
module_exit(helloLKM_exit);
