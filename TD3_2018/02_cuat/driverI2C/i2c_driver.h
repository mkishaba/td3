#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/of.h>
#include <linux/of_irq.h>
#include <linux/of_platform.h>
#include <linux/of_address.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include <linux/delay.h>
#include "../common/td3_ioctl.h"

#define PRIMER_MINOR 0
#define CANT_MINORS  1
#define CLASS_NAME   "td3_dev"
#define DEVICE_NAME  "td3_i2c"
#define BMP180_ID    0x77

/******************************
  Registros del SITARA AM335
******************************/
#define CM_PER                    0x44E00000 // CM_PER (Clock Module Peripheral Registers) (ver pag. 179 y 1250)
#define CM_PER_SIZE               0x400      // Tamaño CM_PER
#define CM_PER_I2C2_CLKCTRL       0x44       // Offset al registro de control del clock del i2c2

#define CONTROL_MODULE_REGS       0x44E10000 // CONTROL MODULE REGISTERS (ver pag. 180)
#define CONTROL_MODULE_REGS_SIZE  0x2000     // Tamaño CONTROL_MODULE_REGS
  #define PIN_CONF_UART1_CTSN     0x978      // conf_uart1_ctsn
  #define PIN_CONF_UART1_RTSN     0x97C      // conf_uart1_rtsn

#define I2C_REGS                  0x4819C000 // Registros del I2C_2 (ver pag. 183)
#define I2C_REGS_SIZE             0x1000     // Tamaño I2C_REGS

#define I2C_CON                   0xA4       // I2C Configuration Register
#define I2C_PSC                   0xB0       // I2C Clock Prescaler Register
#define I2C_SCLL                  0xB4       // I2C SCL Low Time Register
#define I2C_SCLH                  0xB8       // I2C SCL High Time Register
#define I2C_OA                    0xA8       // I2C Own Address Register
#define I2C_SYSC                  0x10       // System Configuration Register
#define I2C_SA                    0xAC       // I2C Slave Address Register

#define I2C_CNT                   0x98       // Data Counter Register (Abajo, la descripción de algunos de los bits)
  #define EN                      0x8000     // Habilitación del módulo I2C (ENABLE)
  #define MST                     0x0400     // MST=1(Modo Master) MST=0(Modo Slave)
  #define TX                      0x0200     // TX=1(Modo transmisión) TX=0(Modo recepción)
  #define STT                     0x0001     // STT(Start Condition) Setearla a 1 para generar la condición de start
  #define STP                     0x0002     // STP(Stop Condition) Setearla a 1 para que se genere la condición de stop 
                                             // cuando DCONT = 0 (finalización de la transmisión/recepción de bytes)

#define I2C_IRQSTATUS_RAW         0x24       // I2C Status Raw Register
  #define BB                      0x1000     // bit de I2C_IRQSTATUS_RAW (BB=0(busFree) BB=1(busOccupied))

#define I2C_IRQENABLE_SET         0x2C       // I2C Interrupt Enable Set Register
#define I2C_IRQENABLE_CLR         0x30       // I2C Interrupt Enable Clear Register
  #define RRDY_IE                 0x08       // Habilitación interrupción de Rx (dato disponible para leer)
  #define XRDY_IE                 0x10       // Habilitación interrupción de Tx (buffer tx disponible para enviar datos)

#define I2C_IRQSTATUS             0x28       // I2C Status Register
  #define RRDY                    0x08       // Dato disponible para lectura
  #define XRDY                    0x10       // Transmisor libre para transmitir

#define I2C_DATA                  0x9C       // Data Access Register (Registro de escritura/lectura de los datos I2C)


/*************************
  Prototipos de funciones
**************************/
int i2c_open(struct inode* nodePath, struct file* filePtr);
int i2c_close(struct inode* nodePath, struct file* filePtr);
ssize_t i2c_read(struct file* filePtr, char __user* userBuff, size_t size, loff_t* offset);
ssize_t i2c_write(struct file* filePtr, const char __user* userBuff, size_t size, loff_t* offset);
static int i2c_probe(struct platform_device* pdev);
static int i2c_remove(struct platform_device* pdev);
static long i2c_ioctl(struct file* filePtr, unsigned int comando, unsigned long arg);
irqreturn_t i2c_irq_handler(int irq, void *dev_id, struct pt_regs *regs);

// Estructura para los buffers de transmisión y recepción del módulo I2C
struct i2cBuff {
  int cantidadDeBytes;
  int indiceBuff;
  char buff[2048];
};
