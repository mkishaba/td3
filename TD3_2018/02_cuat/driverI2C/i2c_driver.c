#include "i2c_driver.h"

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Martín Kishaba");
MODULE_VERSION("1.0");
MODULE_DESCRIPTION("Driver i2c para td3");

dev_t majorMinorNum;   // En esta variable se va a guardar el número mayor y menor del driver
struct cdev *i2c_cdev; // Puntero a la estructura "cdev" (char device) para registrar
                       // el driver en el kernel como un char device
static struct class  *i2cClass;
static struct device *i2cDevice;

struct file_operations i2c_fops = {
  .owner   = THIS_MODULE,
  .read    = i2c_read,
  .write   = i2c_write,
  .open    = i2c_open,
  .release = i2c_close,
  .unlocked_ioctl = i2c_ioctl,
};

// Estructuras para el platform driver - DeviceTree
static const struct of_device_id i2c_match[] = {
  { .compatible = "td3,omap4-i2c" },
  {}
};

MODULE_DEVICE_TABLE(of, i2c_match);

static struct platform_driver i2c_driver = {
  .probe  = i2c_probe,
  .remove = i2c_remove,
  .driver = {
    .name = "td3_i2c",
    .of_match_table = i2c_match
  }
};

static int __init i2c_init(void) {
  int res;

  printk(KERN_ALERT "## ENTRO A LA FUNCION i2c_init ##\n");
  // Primero pido al kernel un número mayor libre
  printk(KERN_ALERT "Obteniendo el número mayor en forma dinámica\n");
  res = alloc_chrdev_region(&majorMinorNum, PRIMER_MINOR, CANT_MINORS, DEVICE_NAME);
  if (res < 0) {
    // Error al obtener los números mayor y menor
    printk(KERN_ALERT "Error al obtener el número mayor del driver!!\n");
    return -1;
  }

  printk(KERN_ALERT "Número MAJOR=%d Número MINOR=%d\n", MAJOR(majorMinorNum), MINOR(majorMinorNum));

  // Pido memoria para la estructura "cdev" del char device y cargo parámetros
  i2c_cdev = cdev_alloc();
  i2c_cdev->ops   = &i2c_fops;     // Lista con las operaciones del driver a implementar
  i2c_cdev->owner = THIS_MODULE;   // Owner de la estructura (nuestro módulo)
  i2c_cdev->dev   = majorMinorNum; // Número mayor-menor pedido anteriormente al kernel 

  // Registración del device
  res = cdev_add(i2c_cdev, majorMinorNum, 1);
  if (res < 0) {
    // No se pudo registrar el device
    unregister_chrdev_region(majorMinorNum, 1);
    printk(KERN_ALERT "No se pudo registrar el device td3_i2c!\n");
    return -1;
  }
  
  // Creación del nodo "td3_i2c" en el directorio "/dev"
  // Hay que crear primero la clase y luego el device.
  // Ver ejemplo en: http://derekmolloy.ie/writing-a-linux-kernel-module-part-2-a-character-device

  // Creación de la clase:
  i2cClass = class_create(THIS_MODULE, CLASS_NAME);
  if (IS_ERR(i2cClass)) {
    cdev_del(i2c_cdev);
    unregister_chrdev_region(majorMinorNum, CANT_MINORS);
    printk(KERN_ALERT "No se pudo crear la clase \"%s\"!\n", CLASS_NAME);
    return PTR_ERR(i2cClass);
  }
  // Creación del device:
  i2cDevice = device_create(i2cClass, NULL, majorMinorNum, NULL, DEVICE_NAME);
  if (IS_ERR(i2cDevice)) {
    class_destroy(i2cClass);
    cdev_del(i2c_cdev);
    unregister_chrdev_region(majorMinorNum, CANT_MINORS);
    printk(KERN_ALERT "No se pudo crear la clase \"%s\"!\n", CLASS_NAME);
    return PTR_ERR(i2cDevice);
  }

  // registro el platform driver para que se ejecute la función probe
  res = platform_driver_register(&i2c_driver);

  if (res != 0) {
    device_destroy(i2cClass, majorMinorNum);
    class_unregister(i2cClass);
    class_destroy(i2cClass);
    cdev_del(i2c_cdev);
    unregister_chrdev_region(majorMinorNum, 1);
    return -1;
  }

  return 0;
}

static void __exit i2c_exit(void) {
  printk(KERN_ALERT "## ENTRO A LA FUNCION i2c_exit ##\n");
  platform_driver_unregister(&i2c_driver);
  device_destroy(i2cClass, majorMinorNum);
  class_unregister(i2cClass);
  class_destroy(i2cClass);
  cdev_del(i2c_cdev);
  unregister_chrdev_region(majorMinorNum, 1);
  printk(KERN_ALERT "Recursos del driver liberados, desintalación del módulo completa!\n");
}

int i2c_virq;                   // Identificador de la interrupción a registrar/solicitar
void *CM_PER_ptr;               // Puntero a los registros CM_PER (Clock Module Peripheral Registers)
void *I2C_REGS_ptr;             // Puntero a los registros del módulo I2C
void *CONTROL_MODULE_REGS_ptr;  // Puntero a los registros MODULE CONTROL REGISTERS
int frecuenciaClockI2c;         // Frecuencia del clock i2c obtenida del deviceTree

static int i2c_probe(struct platform_device* pdev) {
  int res;
  int reintentos=0;
  int *auxPtr;
  
  printk(KERN_ALERT "## ENTRO A LA FUNCION i2c_probe ##\n");
  
  printk(KERN_ALERT "Obtengo la interrupción (VIRQ) desde el DeviceTree \n");
  i2c_virq = platform_get_irq(pdev, 0);
  if (i2c_virq == 0) {
    platform_driver_unregister(&i2c_driver);
    device_destroy(i2cClass, majorMinorNum);
    class_unregister(i2cClass);
    class_destroy(i2cClass);
    cdev_del(i2c_cdev);
    unregister_chrdev_region(majorMinorNum, 1);
    printk(KERN_ALERT "Error al obtener VIRQ!\n");
    return -1;
  }
  printk(KERN_ALERT "VIRQ obtenida:%d\n", i2c_virq);

  // Vinculo rutina de interrupción con VIRQ
  res = request_irq(i2c_virq, (irq_handler_t) i2c_irq_handler, IRQF_TRIGGER_RISING, pdev->name, NULL);
  if (res != 0) {
    platform_driver_unregister(&i2c_driver);
    device_destroy(i2cClass, majorMinorNum);
    class_unregister(i2cClass);
    class_destroy(i2cClass);
    cdev_del(i2c_cdev);
    unregister_chrdev_region(majorMinorNum, 1);
    printk(KERN_ALERT "Error al registrar handler de interrupción!\n");
    return -1;
  } 
  printk(KERN_ALERT "Interrupción inicializada correctamente\n");

  // Intento leer la frecuencia del clock i2c desde el deviceTree
  auxPtr = (void *)of_get_property(pdev->dev.of_node, "clock-frequency", NULL);
  if (auxPtr == NULL) {
    printk(KERN_ALERT "Error al obtener la frecuencia del clock del deviceTree\n");
    return -1;
  }

  // Obtengo la frecuencia de trabajo para el i2c del deviceTree
  frecuenciaClockI2c = be32_to_cpup(auxPtr); // Convierto lo leído del deviceTree a formato int
  printk(KERN_ALERT "Frecuencia del Clock:%dhz\n", frecuenciaClockI2c);

 
  // Pido la zona de memoria correspondiente a los registros de control del clock
  CM_PER_ptr = ioremap(CM_PER, CM_PER_SIZE);
  if (CM_PER_ptr == NULL) {
    printk(KERN_ALERT "No se pudo obtener la zona de memoria CM_PER!\n");
    return -1;
  }

  // Pido la zona de memoria correspondiente a los registros del módulo I2C
  I2C_REGS_ptr = ioremap(I2C_REGS, I2C_REGS_SIZE);
  if (I2C_REGS_ptr == NULL) {
    iounmap(CM_PER_ptr);
    printk(KERN_ALERT "No se pudo obtener la zona de memoria de los registros I2C!\n");
    return -1;
  }

  // Pido la zona de memoria correspondiente a los "CONTROL MODULE REGISTERS"
  // Esto lo pido para configurar el pin_mux (funcionalidad de los pines)
  CONTROL_MODULE_REGS_ptr = ioremap(CONTROL_MODULE_REGS, CONTROL_MODULE_REGS_SIZE);
  if (CONTROL_MODULE_REGS_ptr == NULL) {
    iounmap(CM_PER_ptr);
    iounmap(I2C_REGS_ptr);
    printk(KERN_ALERT "No se pudo obtener la zona de memoria CONTROL MODULE\n");
    return -1;
  }

  // Habilito el clock del módulo i2c (ver pág. 1267)
  iowrite32(0x2, CM_PER_ptr + CM_PER_I2C2_CLKCTRL);
  res = ioread32(CM_PER_ptr + CM_PER_I2C2_CLKCTRL);
  while (reintentos++ < 5) {
    if (res == 2) break;
    else res = ioread32(CM_PER_ptr + CM_PER_I2C2_CLKCTRL);
  }
  
  if (reintentos == 5) {
    printk(KERN_ALERT "No se pudo inicializar el clock i2c\n");
    return -1;
  }
  else {
    printk(KERN_ALERT "Clock i2c inicializado correctamente\n");
  }

  printk(KERN_ALERT "## SALIO CORRECTAMENTE DE i2c_probe ##\n");

  return 0;
}

static int i2c_remove(struct platform_device* pdev) {
  printk(KERN_ALERT "## ENTRO A LA FUNCION i2c_remove ##\n");
  free_irq(i2c_virq, NULL); 
  iounmap(CM_PER_ptr);
  iounmap(I2C_REGS_ptr);
  iounmap(CONTROL_MODULE_REGS_ptr);
  return 0;
}

int i2c_open(struct inode* nodePath, struct file* filePtr) {
  char cuentaClock;

  // De la hoja de datos + el valor de la frecuencia de clock obtenida del deviceTree
  // calculo la cuenta a cargar en los registros SCLL y SCLH para setear el clock
  // ICLK = 12Mhz (lo seteo con el pre-scaler (divisor x 4))
  cuentaClock = (char)((12000000L/(frecuenciaClockI2c*2))-7);
  printk(KERN_ALERT "Cuenta = %d\n", cuentaClock);

  // Configuro los pines físicos del I2C2 para que se comporten como i2c_clock e i2c_data
  // Se los setea modo3 (I2C) y que tienen pull-up
  iowrite32 (0x23, CONTROL_MODULE_REGS_ptr + PIN_CONF_UART1_CTSN);
  iowrite32 (0x23, CONTROL_MODULE_REGS_ptr + PIN_CONF_UART1_RTSN);

  // Inicialización de los registros I2C para poder empezar a usuarlo
  iowrite32(0, I2C_REGS_ptr + I2C_CON);             // Mantiene el modulo en reset
  iowrite32(3, I2C_REGS_ptr + I2C_PSC);             // Setea el divisor del pre-scaler 
                                                    // (div. por (3+1) para obtener 48Mhz/4=12Mhz de clock i2c)
  iowrite32(cuentaClock, I2C_REGS_ptr + I2C_SCLL);  // Tiempo del estado low del clock del I2C
  iowrite32(cuentaClock, I2C_REGS_ptr + I2C_SCLH);  // Tiempo del estado high del clock del I2C
  iowrite32(0x36,I2C_REGS_ptr + I2C_OA);            // Address del módulo I2C (master)
  iowrite32(0, I2C_REGS_ptr + I2C_SYSC);            // System Configuration Register (configuraciones varias del módulo)
  iowrite32(BMP180_ID, I2C_REGS_ptr + I2C_SA);      // Address del esclavo (para nuestro caso es el BMP_180)
  iowrite32(0x8000, I2C_REGS_ptr + I2C_CON);        // Habilitación del módulo I2C

  return 0;
}

int i2c_close(struct inode* nodePath, struct file* filePtr) {
  return 0;
}

struct i2cBuff rxBuffData;                   // Buffer de recepción del módulo I2C
static DECLARE_WAIT_QUEUE_HEAD (i2cRxQueue); // Wait queue para el i2c_read
static int cond_wake_up_rx = 0;              // Flag para levantar el proceso que llamo al driver
unsigned long cantidadDeReads = 0;           // Contador de la cantidad de veces que se llamo a read

ssize_t i2c_read(struct file* filePtr, char __user* userBuff, size_t size, loff_t* offset) {

  int tout = 1000;      // Tiempo de espera a que el bus esté libre
  int cantidadErrores;
 
  // Verifico que el buffer de usuario apunte a un área de memoria válida
  if (!access_ok(VERYFY_WRITE, userBuff, size)) {
    printk(KERN_ALERT "Buffer de usuario no válido!\n");
    return -1;
  }

  rxBuffData.cantidadDeBytes = size;                   // Cantidad de bytes a leer
  rxBuffData.indiceBuff = 0;                           // Índice del buffer de recepción
  iowrite32(size, (void *)(I2C_REGS_ptr + I2C_CNT));   // Indico la cantidad de bytes a leer en el registro I2C_CNT

  // Chequeo que el bus i2c esté libre leyendo el bit 'BB' del registro "I2C Status Raw Register"
  while ((ioread32(I2C_REGS_ptr + I2C_IRQSTATUS_RAW) & BB) && tout) {
    tout--;
    udelay(1);
  } 

  if (tout == 0) {
    printk(KERN_ALERT "Bus I2C ocupado (i2c_read)\n");
    return -1;
  } 
  else {
    iowrite32(EN|MST|STP|STT, I2C_REGS_ptr + I2C_CON);    // Habilito la recepción de datos
    iowrite32(RRDY_IE, I2C_REGS_ptr + I2C_IRQENABLE_SET); // Habilito interrupción de rx

    // Mando a dormir al proceso, a la espera de que termine la lectura de datos por I2C
    wait_event_interruptible(i2cRxQueue, cond_wake_up_rx == 1);
    
    // Se despierta el proceso (lo despierta la rutina de interrupción al finalizar la recepción de datos)
    cond_wake_up_rx = 0; // Limpio la condición de wake up

    // Copio los datos leídos al buffer de usuario
    cantidadErrores = copy_to_user(userBuff, rxBuffData.buff, size);
    if (cantidadErrores > 0) {
      printk(KERN_ALERT "Error en copy_to_user (i2c_read))\n");
      return -1;   
    } 
  }
  cantidadDeReads++;
  return 0;
}

struct i2cBuff txBuffData;                   // Buffer de transmisión del módulo I2C
static DECLARE_WAIT_QUEUE_HEAD (i2cTxQueue); // Wait queue para el i2c_write
static int cond_wake_up_tx = 0;              // Flag para levantar el proceso que llamo al driver

ssize_t i2c_write(struct file* filePtr, const char __user* userBuff, size_t size, loff_t* offset) {
  
  int tout = 1000;      // Tiempo de espera a que el bus esté libre
  int cantidadErrores;

  // Verifico que el buffer de usuario apunte a un área de memoria válida
  if (!access_ok(VERYFY_READ, userBuff, size)) {
    printk(KERN_ALERT "Buffer de usuario no válido!\n");
    return -1;
  }

  // Copio al buffer de tx los datos a escribir desde el buffer del usuario
  cantidadErrores = copy_from_user(txBuffData.buff, userBuff, size);
  if (cantidadErrores > 0) {
    printk(KERN_ALERT "Error en copy_from_user (i2c_read))\n");
    return -1;
  }

  txBuffData.cantidadDeBytes = size;                   // Cantidad de bytes a leer
  txBuffData.indiceBuff = 0;                           // Índice del buffer de recepción
  iowrite32(size, I2C_REGS_ptr + I2C_CNT);             // Indico la cantidad de bytes a leer en el registro I2C_CNT

  // Chequeo que el bus i2c esté libre leyendo el bit 'BB' del registro "I2C Status Raw Register"
  while ((ioread32(I2C_REGS_ptr + I2C_IRQSTATUS_RAW) & BB) && tout) {
    tout--;
    udelay(1);
  } 

  if (tout == 0) {
    printk(KERN_ALERT "Bus I2C ocupado (i2c_write)\n");
    return -1;
  } 
  else {
    iowrite32(EN|MST|TX|STP|STT, I2C_REGS_ptr + I2C_CON); // Habilito la transmisión de datos
    iowrite32(XRDY_IE, I2C_REGS_ptr + I2C_IRQENABLE_SET); // Habilito interrupción de tx

    // Mando a dormir al proceso, a la espera de que termine la lectura de datos por I2C
    wait_event_interruptible(i2cTxQueue, cond_wake_up_tx == 1);
    
    // Se despierta el proceso (lo despierta la rutina de interrupción al finalizar la transmisión de datos)
    cond_wake_up_tx = 0; // Limpio la condición de wake up
  }
  return 0;
}

irqreturn_t i2c_irq_handler (int irq, void *dev_id, struct pt_regs *regs) {
  int irqStatus;
  
  printk(KERN_ALERT "Entro en la interrupción\n");
  irqStatus = ioread32(I2C_REGS_ptr + I2C_IRQSTATUS);        // Leo el registro de status de las interrupciones
  iowrite32((int)(RRDY|XRDY), I2C_REGS_ptr + I2C_IRQSTATUS); // Limpio los flags de las interrupciones
  
  // Interrupción por recepción de datos
  if (irqStatus & RRDY) {
    // Leo el dato que llego y aumento el índice del buffer
    rxBuffData.buff[rxBuffData.indiceBuff] = ioread32(I2C_REGS_ptr + I2C_DATA);
    rxBuffData.indiceBuff++;

    // Controlo si se leyeron todos los bytes de datos
    if (rxBuffData.indiceBuff == rxBuffData.cantidadDeBytes) {
      // Deshabilito la interrupción de rx
      iowrite32(RRDY_IE, I2C_REGS_ptr + I2C_IRQENABLE_CLR);
      // Levanto el proceso que se fue a dormir
      cond_wake_up_rx = 1;
      wake_up_interruptible(&i2cRxQueue);  
    }
    return IRQ_HANDLED;
  }

  // Interrupción por transmisión de datos
  if (irqStatus & XRDY) {
    // Escribo los datos a transmitir y aumento el índice del buffer de datos
    iowrite32(txBuffData.buff[txBuffData.indiceBuff], I2C_REGS_ptr + I2C_DATA);
    txBuffData.indiceBuff++;

    // Controlo si se transmitieron todos los bytes de datos
    if (txBuffData.indiceBuff == txBuffData.cantidadDeBytes) {
      // Deshabilito la interrupción de tx
      iowrite32(XRDY_IE, I2C_REGS_ptr + I2C_IRQENABLE_CLR);
      // Levanto el proceso que se fue a dormir
      cond_wake_up_tx = 1;
      wake_up_interruptible(&i2cTxQueue);  
    }
    return IRQ_HANDLED;
  }
  return IRQ_HANDLED;
}

static long i2c_ioctl(struct file* filePtr, unsigned int comando, unsigned long arg) {
  printk(KERN_ALERT "Entró en i2c_ioctl\n");
  switch(comando) {
    case TD3_I2C_CANTIDAD_LECTURAS: // Comando que devuelve por ioctl la cantidad de veces que se leyo el parámetro
                                    // desde que se instaló el driver
      // Cada vez que leo el parámetro del sensor a informar se realizan 3 reads
      return cantidadDeReads/3;
    default:
      return -ENOTTY;
  }
}

module_init(i2c_init);
module_exit(i2c_exit);
