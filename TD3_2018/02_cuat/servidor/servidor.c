#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include "servidor.h"
#include "../common/conf.h"
#include "../tcpSockets/tcpSockets.h"
#include "../bmp180/bmp180.h"
#include "../sharedMemory/sharedMemory.h"
#include "../semaphore/semaphore.h"
#include "../common/td3_ioctl.h"

int cantidadDeConexionesAbiertas=0;  // Cantidad de conexiones abiertas

// Handler de la señal SIGCHLD
void sigchld_handler(int s)
{
  // como waitpid puede sobreescribir errno, lo guardamos y luego lo recuperamos
  int saved_errno = errno;

  // pid_t waitpid(pid_t pid, int *status, int options);
  // La función se queda esperando (suspende la ejecución del proceso invocante) hasta que uno de sus hijos finalice.
  // Si no se realiza esta espera, no se liberan correctamente los recursos del proceso hijo quedando en estado ZOMBIE
  //   pid = -1: esperamos por cualquier proceso hijo
  //   status = NULL: no guardamos información de status del proceso hijo que termina
  //   options = WNOHANG: la función retorna inmediatamente si no ha terminado ningún hijo (la función retorna 0)
  while(waitpid(-1, NULL, WNOHANG) > 0);
  errno = saved_errno;

  if (cantidadDeConexionesAbiertas) {
    cantidadDeConexionesAbiertas--;  
  }
}

void *threadLecturaTemperatura(void *arg) {
  float temperatura=0;
  float presion=0;
  int fd;
  float *parentShmPtr; // Puntero a la memoria compartida
  int parentSemIdRef;  // Id del semáforo de R/W de la sharedMemory
  int cantidadDeLecturas;

  // Creo el semáforo de la sharedMemory
  parentSemIdRef = creaSetSemaforos(CLAVE_SEMAFORO_SHR_MEM, CANT_SEMAFOROS_SHR_MEM);

  // Hago el attach a la memoria compartida
  parentShmPtr = (float *) attachMemoriaCompartida(CLAVE_MEMORIA_COMPARTIDA);
  
  // Inicializo el sensor de temperatura
  fd = sensorInit();

  // Si falló la inicialización del sensor o el attach a la memoria compartida o la creación del semáforo
  // finalizo el programa
  if (fd > 0 && parentShmPtr != NULL && parentSemIdRef != -1) {
    while(1) {
      // Leo la temperatura
      leeTemperaturaYPresion(fd, &temperatura, &presion);

      // Trato de lockear el semáforo para escribir la sharedMemory
      if (pideSemaforo(parentSemIdRef, 0) == -1) {
        printf("Error al lockear semáforo! (en el thread)\n");
        exit(-1);
      }

      // Actualizo la memoria compartida
      parentShmPtr[0] = temperatura; 

      // Libero el semáforo
      if (liberaSemaforo(parentSemIdRef, 0) == -1) {
        printf("Error al liberar semáforo! (en el thread)\n");
        exit(-1);
      }

      // Leo la cantidad de lecturas que realizó el driver desde
      // su instalación
      cantidadDeLecturas = ioctl(fd, TD3_I2C_CANTIDAD_LECTURAS);

      printf("Temp.   %.1f °C\n", temperatura);
      printf("Presion %.3f hPa\n", presion);
      printf("Cantidad de lecturas desde la instalación del driver:%ld\n", cantidadDeLecturas);
 
      // Leo el sensor cada 1 segundo
      sleep(1);
    }
  }
  else {
    printf("Error al inicializar el thread de lectura de temperatura!\n");
    exit(-1);
  }
}

void creaThreadLecturaTemperatura(void) {
  pthread_t pthread_id;
  char arg;
  int iret;

  iret = pthread_create(&pthread_id, NULL, threadLecturaTemperatura, (void*) &arg);
  if(iret) {
    printf("Error al crear thread!!");
    exit(EXIT_FAILURE);
  }
}

int main(void) {
  int socketServidor;
  int socketNuevaConexion;
  int cantidadDeConexiones = 0;
  struct sockaddr_storage infoCliente;
  socklen_t infoClienteSize;
  char addressNuevaConexion[INET6_ADDRSTRLEN];
  struct sigaction sa;
  char buf[CANT_MAX_BYTES];
  struct timeval tv;
  fd_set readfds;
  int res;
  time_t t;
  struct tm tm;

  // Modifico el handler de la señal SIGCHLD
  // Para finalizar correctamente los procesos hijos
  sa.sa_handler = sigchld_handler;
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = SA_RESTART;
  if (sigaction(SIGCHLD, &sa, NULL) == -1) {
    perror("sigaction");
    exit(-1);
  }

  // Creo el thread que va a leer la temperatura periódicamente
  creaThreadLecturaTemperatura();
 
  // Creo el socket y lo bindeo al puerto PUERTO_SERVIDOR
  socketServidor = creaSocketTcpYBindea(PUERTO_SERVIDOR);
  if (socketServidor == -1) {
    printf("Error: socketServidor\n");
    return -1;
  }

  // Me pongo a escuchar conexiones. Encolo hasta una cantidad BACKLOG conexiones
  if (listen(socketServidor, BACKLOG)) {
    perror("listen");
    return -1;
  }

  printf("Servidor: esperando conexiones...\n");

  // Loop principal del servidor
  while(1) {
    infoClienteSize = sizeof(infoCliente);

    if (cantidadDeConexionesAbiertas < 1000) {
      // Me quedo esperando nuevas conexiones
      socketNuevaConexion = accept(socketServidor, (struct sockaddr *) &infoCliente, &infoClienteSize);

      if (socketNuevaConexion == -1) {
        perror("accept socketNuevaConexion");
        continue;
      }

      // Actualizo el contador de conexiones abiertas
      cantidadDeConexionesAbiertas++;  

      // Obtengo el string de la IP de la conexión entrante
      inet_ntop(infoCliente.ss_family, 
                get_in_addr((struct sockaddr *) &infoCliente),
                addressNuevaConexion,
                sizeof(addressNuevaConexion));
      printf("Servidor: nueva conexión desde %s\n", addressNuevaConexion);

      // Creo un nuevo proceso para atender la conexión entrante
      if (!fork()) {
        // En el proceso hijo
        close(socketServidor); // El hijo no usa este socket, lo cierro
        if (send(socketNuevaConexion, "Hola!", 13, 0) == -1) {
          perror("send1");
        }
        else {
          float *childShmPtr;                        // Puntero a la memoria compartida
          float temperaturaLeida;                    // Buffer de lectura de al temperatura
          int childSemIdRef;                         // semId al semáforo para la R/W de la sharedMemory
          FD_ZERO(&readfds);                         // Limpio el set que voy a usar en la función select()
          FD_SET(socketNuevaConexion, &readfds);     // Agrego al set el socket a escuchar 

          // Hago el attach a la memoria compartida
          childShmPtr = (float *) attachMemoriaCompartida(CLAVE_MEMORIA_COMPARTIDA);

          if (childShmPtr == NULL) {
            printf("Error en el attach a la memoria compartida (proceso hijo)\n");
            close(socketNuevaConexion);
            return -1;
          }

          // Pido una referencia al semáforo de la sharedMemory
          childSemIdRef = pideIdSetSemaforos(CLAVE_SEMAFORO_SHR_MEM);
          if (childSemIdRef == -1) {
            printf("Error al obtener el id del semáforo (proceso hijo)\n");
            close(socketNuevaConexion);
            return -1;
          }

          // Loop comunicación proceso hijo
          while(1) {
            tv.tv_sec = TIMEOUT_CONEXION; // Si no me llega en 5 segundos la respuesta del servidor, corto la conexión
            tv.tv_usec = 0;
            // Blockea hasta que haya una actualización en el socketNuevaConexion
            select(socketNuevaConexion+1, &readfds, NULL, NULL, &tv);
            
            if (FD_ISSET(socketNuevaConexion, &readfds)) {
              memset(buf, 0, sizeof(buf));
              res = recv(socketNuevaConexion, buf, CANT_MAX_BYTES-1, 0);
              if (res == -1) {
                perror("Servidor: recv1");
                close(socketNuevaConexion);
                return -1;
              }

              if (res == 0) {
                // Si el resultado de recv() es 0, significa que se cerro el socket desde el lado del cliente
                printf("Se cerró la conexión\n");
                close(socketNuevaConexion);
                return 0;
              }

              if (buf[0] == (char) CMD_TEMPERATURA) {
                t = time(NULL);
                tm = *localtime(&t);

                // Trato de lockear el semáforo para leer la sharedMemory
                if (pideSemaforo(childSemIdRef, 0) == -1) {
                  printf("Error al lockear semáforo! (en el hijo)\n");
                  close(socketNuevaConexion);
                  return -1;
                }

                // Leo la sharedMemory
                temperaturaLeida = childShmPtr[0];

                // Libero el semáforo
                if (liberaSemaforo(childSemIdRef, 0) == -1) {
                  printf("Error al liberar semáforo! (en el hijo)\n");
                  close(socketNuevaConexion);
                  return -1;
                }

                memset(buf, 0, sizeof(buf));
                sprintf(buf, "%d/%d/%d Temperatura=%.1f°C", tm.tm_mday-1, tm.tm_mon+1, tm.tm_year-100, temperaturaLeida);
                res = send(socketNuevaConexion, buf, strlen(buf), 0);
                if (res == -1) {
                  perror("send2");
                  close(socketNuevaConexion);
                  return -1;
                }
              } else {
                printf("No se entendio el mensaje:0x%x\n", buf[0]);
              }
            }
            else {
              // Sale por timeout de conexión
              printf("Timeout socketNuevaConexion\n");
              close(socketNuevaConexion);
              return 0;
            }
          }
        }
        close(socketNuevaConexion);
        return 0;
      }
    }
  }
  close(socketServidor);
  return 0;
}
