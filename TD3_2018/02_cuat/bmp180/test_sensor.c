#include <stdio.h>
#include <unistd.h>
#include "./bmp180.h"

// Para testear el sensor en forma independientemente
int main(int argc, char *argv[]) { 
  float temperatura = 0;
  float presion = 0;
  int fd;

  fd = sensorInit();
  if (fd > 0) {
    leeTemperaturaYPresion(fd, &temperatura, &presion);
    printf("Temp.   %.1f C\n", temperatura);
    printf("Presion %.3f hPa\n", presion);
  } else {
    printf("Error al inicializar el sensor!\n");
    return -1;
  }
  close(fd);
  return(0);
}
