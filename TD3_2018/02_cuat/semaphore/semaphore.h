#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

int creaSetSemaforos(int clave, int cantidadSemaforos);
int borraSetSemaforos(int semid);
int pideIdSetSemaforos(int clave);
int pideSemaforo(int semid, int sem_num);
int liberaSemaforo(int semid, int sem_num);

union semun {
  int val;
  struct semid_ds *buf;
  ushort *array;
};
