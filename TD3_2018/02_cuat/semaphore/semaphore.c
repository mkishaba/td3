#include <stdio.h>
#include "semaphore.h"

// Función para crear un set de semáforos a partir de una clave del tipo char
int creaSetSemaforos(int clave, int cantidadSemaforos) {
  key_t key;
  int semId;
  union semun arg;
  struct sembuf sb;

  // Obtengo clave para crear el semáforo
  key = ftok(".", clave);
  if (key == -1) {
    printf("Error al obtener clave (ftok)(creaSetSemaforos)\n");
    return -1;
  };

  // Creo el set de semáforos.
  semId = semget(key, cantidadSemaforos, IPC_CREAT | 0666);
  if (semId == -1) {
    printf("Error al crear set de semáforos!\n");
    return -1;
  }

  // Inicializo todos los semáforos para poder usarlos
  sb.sem_op = 1;
  sb.sem_flg = 0;
  arg.val = 1;
  for(sb.sem_num = 0; sb.sem_num < cantidadSemaforos; sb.sem_num++) { 
    if (semop(semId, &sb, 1) == -1) {
      // Sí falló borro el semáforo y salgo por error
      borraSetSemaforos(semId); 
      printf("Error al inicializar semáforos\n");
      return -1;
    }
  }
  return semId;
}

// Devuelve el semId de un set de semáforos a partir de una clave
int pideIdSetSemaforos(int clave) {
  key_t key;
  int semId;

  // Obtengo clave para crear el semáforo
  key = ftok(".", clave);
  if (key == -1) {
    printf("Error al obtener clave (ftok)(pideIdSetSemaforos)\n");
    return -1;
  };

  // Obtengo el semId a partir de la key generada
  semId = semget(key, 0, 0);
  if (semId == -1) {
    printf("Error al obtener semid del set de semáforos!\n");
    return -1;
  }
  return semId;
}

// Función para borrar un set de semáforos a partir del semId
int borraSetSemaforos(int semId) {
  key_t key;

  // Borro el set de semáforos. El 2do parámetro (semnum) de la función semctl es ignorado
  if (semctl(semId, 0, IPC_RMID) == -1) {
    printf("Error al borrar set de semáforos!\n");
    return -1;
  } 
  return 0;
}

// Función para intentar lockear el semáforo. Si está tomado, bloquea la ejecución (duerme el proceso)
int pideSemaforo(int semId, int sem_num) {
  struct sembuf sb;

  sb.sem_num = sem_num;
  sb.sem_op = -1;
  sb.sem_flg = SEM_UNDO;

  if (semop(semId, &sb, 1) == -1) {
    printf("Error al intentar lockear el semáforo");
    return -1;
  }
  return 0;
}

// Función para liberar el semáforo
int liberaSemaforo(int semId, int sem_num) {
  struct sembuf sb;

  sb.sem_num = sem_num;
  sb.sem_op = 1;
  sb.sem_flg = SEM_UNDO;

  if (semop(semId, &sb, 1) == -1) {
    printf("Error al liberar el semáforo");
    return -1;
  }
  return 0;
}
