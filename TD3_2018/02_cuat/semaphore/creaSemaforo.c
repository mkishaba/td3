#include <stdio.h>
#include "semaphore.h"

int main(void) {
  int semId;

  if ((semId = creaSetSemaforos('E', 1)) == -1) {
    printf("Error al crear set de semáforos!\n");
    return -1;
  }
  printf("El semáforo se creo correctamente\n");
  return 0;
}
