#include <stdio.h>
#include "semaphore.h"

int main(void) {
  int semId;

  if ((semId = pideIdSetSemaforos('E')) == -1) {
    printf("Error al obtener semId\n");
    return -1;
  }
  
  if (borraSetSemaforos(semId) == -1) {
    printf("Error al borrar set de semáforos!\n");
    return -1;
  }

  printf("El set de semáforos se borro correctamente!\n");
  return 0;
}
