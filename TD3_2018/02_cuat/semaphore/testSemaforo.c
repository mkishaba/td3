#include <stdio.h>
#include "semaphore.h"

int main(void) {
  int semId;

  if ((semId = pideIdSetSemaforos('E')) == -1) {
    printf("Error al obtener semId!\n");
    return -1;
  }

  printf("Se intentará lockear el semáforo (PRESIONE ENTER)\n");
  getchar();
  if (pideSemaforo(semId, 0) == -1) {
    printf("Error al lockear semáforo!\n");
    return -1;
  }

  printf("Semáforo lockeado! Esperando para liberarlo (PRESIONE ENTER)\n");
  getchar();
  if (liberaSemaforo(semId, 0) == -1) {
    printf("Error al liberar semáforo!\n");
    return -1;
  }

  printf("Semáforo liberado!\n");
  return 0;
}
