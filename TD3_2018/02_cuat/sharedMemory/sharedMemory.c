#include "./sharedMemory.h"
#include <stdio.h>

// Está función de vuelve un puntero a memoria compartida
// según la clave que se le pase (2do parámetro de ftok)
char *attachMemoriaCompartida(char clave) {
  key_t keyMemoriaCompartida;
  int shmid;
  char *shmPtr;
  
  if ((keyMemoriaCompartida = ftok(".", clave)) == -1) {
    printf("Error al generar key para la memoria compartida!\n");
    return NULL;
  }

  if ((shmid = shmget(keyMemoriaCompartida, sizeof(float), 0644 | IPC_CREAT)) == -1) {
    printf("Error al crear memoria compartida!!\n");
    return NULL;
  }

  shmPtr = shmat(shmid, (void *)0, 0);
  if (shmPtr == (char *)(-1)) {
    printf("Error en attach a la memoria compartida!!\n");
    return NULL;
  }
  printf("Memoria compartida inicializada!!\n");
  return shmPtr;
}
