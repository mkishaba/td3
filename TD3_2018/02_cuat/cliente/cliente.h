#include "../common/conf.h"

struct datosThread {
  char ip[20];
  unsigned int periodicidadEncuesta;
  unsigned char nroTerminal;
  char buff[CANT_MAX_BYTES];
  char conectadoAlServer;
};
