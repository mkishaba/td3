#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <pthread.h>
#include "cliente.h"
#include "../tcpSockets/tcpSockets.h"

pthread_mutex_t mutex;

void *threadLecturaTemperatura(void *data) {
  int socketClienteServidor;
  struct timeval tv;
  fd_set readfds;
  int res;
  struct datosThread *datos;
  char ip[20];
  unsigned int periodicidadEncuesta;
  unsigned char nroTerminal;
  char auxBuff[CANT_MAX_BYTES];

  // Actualizo los datos locales con los de visibilidad global
  pthread_mutex_lock(&mutex);
    datos = (struct datosThread *)data;
    strcpy(ip, datos->ip);
    periodicidadEncuesta = datos->periodicidadEncuesta;      
    nroTerminal = datos->nroTerminal;
  pthread_mutex_unlock(&mutex);
  
  // Crea el socket de conexión al servidor
  socketClienteServidor = creaSocketTcpYConecta(ip, PUERTO_SERVIDOR);
  if (socketClienteServidor == -1) {
    perror("Error: socketCliente\n");
    return NULL;
  }

  // Recibo el primer paquete enviado por el servidor
  if ((recv(socketClienteServidor, auxBuff, CANT_MAX_BYTES-1, 0)) == -1) {
    perror("recv1");
    return NULL;
  } else {

    // Actualizo el estado del servidor a conectado
    pthread_mutex_lock(&mutex);
      datos->conectadoAlServer = 1;
    pthread_mutex_unlock(&mutex);

    FD_ZERO(&readfds);                         // Limpio el set que voy a usar en la función select()
    FD_SET(socketClienteServidor, &readfds);   // Agrego al set el socket a escuchar

    // Loop comunicación cliente-servidor
    while(1) {
      memset(auxBuff, 0, sizeof(auxBuff));
      auxBuff[0] = CMD_TEMPERATURA;            // Comando/solicitud a enviar al servidor 
      
      // Pido info al servidor
      res = send(socketClienteServidor, auxBuff, CMD_SIZE, 0);
      if ( res == -1) {
        perror("Cliente: send()");
        pthread_mutex_lock(&mutex);
          datos->conectadoAlServer = 0;
        pthread_mutex_unlock(&mutex);
        return NULL;
      }
 
      tv.tv_sec = TIMEOUT_CONEXION;            // Si no me llega en 5 segundos la respuesta del servidor, corto la conexión
      tv.tv_usec = 0;
      // Blockea hasta que haya una actualización en el socketClienteServidor
      select(socketClienteServidor+1, &readfds, NULL, NULL, &tv);

      if (FD_ISSET(socketClienteServidor, &readfds)) {
        // Llegó el dato solicitado, lo recibo
        res = recv(socketClienteServidor, auxBuff, CANT_MAX_BYTES-1, 0);

        if (res == -1) {
          perror("Cliente: recv2()");
          pthread_mutex_lock(&mutex);
            datos->conectadoAlServer = 0;
          pthread_mutex_unlock(&mutex);
          return NULL;
        }
 
        if (res == 0) {
          // Si el resultado de recv() es 0, significa que se cerro el socket desde el servidor
          perror("Se cerró la conexión\n");
          close(socketClienteServidor);
          pthread_mutex_lock(&mutex);
            datos->conectadoAlServer = 0;
          pthread_mutex_unlock(&mutex);
          return NULL;
        }

        // Actualizo los datos a mostrar por consola para este thread
        pthread_mutex_lock(&mutex);
          strcpy(datos->buff, auxBuff);
        pthread_mutex_unlock(&mutex);
      }
      else {
        // Sale por timeout de conexión
        perror("Timeout socketClienteServidor\n");
        close(socketClienteServidor);
        pthread_mutex_lock(&mutex);
          datos->conectadoAlServer = 0;
        pthread_mutex_unlock(&mutex);
        return NULL;
      }
      sleep(periodicidadEncuesta);
    }
  }
  close(socketClienteServidor);
  pthread_mutex_lock(&mutex);
    datos->conectadoAlServer = 0;
  pthread_mutex_unlock(&mutex);
  return 0;
}

int main(int argc, char *argv[]) {
  pthread_t id[3];
  int i;
  struct datosThread *datosPtr;

  if (argc < 3) {
    printf("Uso: cliente periodicidadEncuesta ipServidor1 ipServidor2 ..... ipServidorN\n");
    return 0;
  }

  // Pido memoria para guardar los datos
  datosPtr = malloc((argc-2)*sizeof(struct datosThread));

  // Actualizo las estructuras de cada thread con la info pasado por parámetros
  for (i=0; i<argc-2; i++) {
    strcpy(datosPtr[i].ip, argv[i+2]);
    datosPtr[i].periodicidadEncuesta = atoi(argv[1]);
    datosPtr[i].nroTerminal = i;
    datosPtr[i].conectadoAlServer = 0;
  } 

  // Inicializo mutex
  pthread_mutex_init(&mutex, NULL);

  // Creo los threads de conexión a cada servidor
  for (i=0; i<argc-2; i++) {
    if (pthread_create(&id[i], NULL, threadLecturaTemperatura, (void *)&datosPtr[i]) != 0) {
      perror("pthread_create");
      exit(-1);
    }
  }

  // Loop del hilo principal que actualiza en pantalla las lecturas de cada estación de
  // medición
  while(1) {
    pthread_mutex_lock(&mutex);
      system("clear");
      printf("\n");
      for (i=0; i<argc-2; i++) {
        printf("Estación %d ip:%s ", datosPtr[i].nroTerminal, datosPtr[i].ip);
        if (datosPtr[i].conectadoAlServer) {
          printf("Lectura: %s\n", datosPtr[i].buff);
        } else {
          printf("Conexión caída\n");
        }
      }
    pthread_mutex_unlock(&mutex);
    usleep(100000);
  }
 
  for (i=0; i<argc-2; i++) {
    pthread_join(id[i], NULL);
  }

  free(datosPtr);
  return 0;
}
