#include <linux/ioctl.h>

// Número mágico que adopto para nuestro i2c device
// Lo ideal es que no esté usado por otro device
// Para eso hay que ver "Documentation/ioctl-number.txt"
#define TD3_I2C_MAGIC 0xfe

// Comandos ioctl del i2c device, uso la forma estandar para definir las macros
#define TD3_I2C_CANTIDAD_LECTURAS _IOR(TD3_I2C_MAGIC, 1, long)

