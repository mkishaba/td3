SECTION .main progbits
GLOBAL main
EXTERN ___tabla_de_digitos
EXTERN ___fin_tabla_de_digitos

USE32
main:
  mov edi, ___tabla_de_digitos           ; Inicializamos el índice a la dirección de inicio de la tabla
  ; Bucle polling de teclado
  .kybrd_poll:
    xor   eax, eax
    in    al,  0x64                      ; Lee el puerto 0x64 (status register del 8042 (controlador de teclado))
    bt    eax, 0x00                      ; Verificamos que haya algo que leer (bit0 = 1 => output buffer full)
  jnc  .kybrd_poll                       ; Vuelve a ejecutar el bucle si no hay nada que leer del buffer de teclado

    in    al, 0x60                       ; Leemos el buffer de teclado
    cmp   al, 0x1f                       ; Chequeamos si se presiono la tecla 's'
  je .fin                                ; Terminamos el programa si se presiono la tecla 's'

    mov bl, al
    and bl, 0x80                         ; Vuelvo al inicio del bucle si es un break code
  jnz .kybrd_poll

    xchg  bx, bx                         ; Magic breakpoint (se presiono una tecla)
    mov   [edi], al                      ; Si no se presiono la tecla 's', guardamos en la tabla la tecla presionada
    inc   edi                            ; Incrementamos el índice (overflow cuando llega a los 64K, buffer circular)
    cmp   edi, ___fin_tabla_de_digitos   ; Verificamos si el índice ya llego a los 64k de la tabla
    jne   .kybrd_poll                    ; Si no se llego al final de la tabla se continua con el polling
    mov   edi, ___tabla_de_digitos       ; Inicializamos el índice a la dirección de inicio de la tabla
  jmp .kybrd_poll

  ; Finalización del programa
  .fin:
    hlt
    jmp .fin

