SECTION .sys_tables progbits alloc noexec nowrite
GLOBAL CS_SEL_32
GLOBAL DS_SEL
GLOBAL GDT_LENGTH
GLOBAL rom_gdtr

GDT:
NULL_SEL    equ $-GDT
   dq 0x0
DS_SEL      equ $-GDT
   dw 0xffff            ; Límite bits 0-15
   dw 0x0000            ; Base bits 0-15
   db 0x00              ; Base bits 16-23
   db 10010010b         ; P=1(Seg. presente), DPL=0, S=1, Bit11=0(datos), W=1(writable)
   db 11001111b         ; G=1(limitex4K), D/B=1(32bits), L=0, AVL=0 / Límite bits 19-16
   db 0                 ; Base bits 31-24
CS_SEL_32   equ $-GDT
   dw 0xffff            ; Límite bits 0-15
   dw 0x0000            ; Base bits 0-15
   db 0x00              ; Base bits 16-23
   db 10011010b         ; P=1(Seg. presente), DPL=0, S=1, Bit11=1(código), R=1(readable)
   db 11001111b         ; G=1(limitex4K), D/B=1(32bits), L=0, AVL=0 / Límite bits 19-16
   db 0                 ; Base bits 31-24
GDT_LENGTH  equ $-GDT

rom_gdtr:
   dw GDT_LENGTH-1      ; Se resta el selector nulo
   dd GDT
