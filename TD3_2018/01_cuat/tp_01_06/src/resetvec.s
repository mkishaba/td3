USE16
EXTERN start16

SECTION .resetvec
reset_vector:
  cli          ; Deshabilita las interrupciones
  cld          ; Limpia el flag DF, esi y edi se incrementan cuando se realizan operaciones de string
  jmp start16  ; Jump al código de la ROM
;align 16      ;Relleno de nop (0x90) resuelto en el enlazador
