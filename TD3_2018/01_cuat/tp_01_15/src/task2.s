EXTERN ___tasks_lineal_bss_st
EXTERN ___tasks_lineal_buffer_tabla
EXTERN HLT_SYS_CALL_SEL
EXTERN READ_SYS_CALL_SEL
EXTERN INDEX_SYS_CALL_SEL

SECTION .task2 progbits
;-------------------------------------------------------------------------------;
;                                  Task 2                                       ;
; Suma de a bytes todos los números de 8 bytes guardados en la tabla de dígitos ;
;-------------------------------------------------------------------------------;
inicioTarea2:
  ; Obtengo el valor del índice de la tabla de dígitos
  call INDEX_SYS_CALL_SEL:0                              ; Carga eax con el valor del índice de la tabla de dígitos
  cmp eax, 0
  je .hltTarea2

  mov ecx, ___tasks_lineal_buffer_tabla                  ; Buffer donde almacenar la tabla de dígitos
  call READ_SYS_CALL_SEL:0                               ; Llamo a td3_read

  cmp eax, 0                                             ; Si no copio nada, hubo algún error
  je .hltTarea2
  
  ; A partir del índice de la tabla de dígitos obtengo 
  ; la cantidad de números almacenados en la tabla de dígitos
  mov ecx, 8
  mov edx, 0                                 
  div ecx
  mov ecx, eax                                           ; Guardo la cantidad de numeros en la tabla

  ; Calculo de la suma
  pxor mm0, mm0                                          ; En mm0 acumulo la suma
  .loopSumaBytes:
    pxor   mm1, mm1                                      ; mm1 lo uso como buffer
    movq   mm1, [___tasks_lineal_buffer_tabla+(ecx-1)*8]
    paddb  mm0, mm1                                      ; voy acumulando la suma en eax (suma en bytes)
  loop .loopSumaBytes

  movq [___tasks_lineal_bss_st], mm0                     ; Guardo el resultado

.hltTarea2:
  call HLT_SYS_CALL_SEL:0
jmp .hltTarea2
