GLOBAL TABLA_ISRs
GLOBAL INDICE_TABLA_DIGITOS

EXTERN ___tabla_de_digitos
EXTERN ___tablas_de_paginacion
EXTERN __set_ptree_entry_32
EXTERN ___task0_cr3
EXTERN ___task1_cr3
EXTERN ___task2_cr3
EXTERN __L3_STACK_LINEAL_ADDR_END
EXTERN __L0_STACK_LINEAL_ADDR_END
EXTERN ___tasks_lineal_addr_st
EXTERN ___tasks_lineal_mmx_data_st
EXTERN USR_RAM_DS_SEL
EXTERN USR_RAM_CS_SEL_32
EXTERN RAM_DS_SEL
EXTERN RAM_CS_SEL_32
EXTERN TAREA_FINALIZADA

SECTION .ISR progbits
;---------------------------------------------------;
; Rutinas de atención de interrupciones/excepciones ;
;---------------------------------------------------;
isr_0:
  xchg bx, bx            ; Magic breakpoint
  mov edx, 0
  .fin_isr0:
    hlt
  jmp .fin_isr0

isr_1:
  xchg bx, bx            ; Magic breakpoint
  mov edx, 1
  .fin_isr1:
    hlt
  jmp .fin_isr1

isr_2:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 2
  .fin_isr2:
    hlt
  jmp .fin_isr2

isr_3:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 3
  .fin_isr3:
    hlt
  jmp .fin_isr3

isr_4:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 4
  .fin_isr4:
    hlt
  jmp .fin_isr4

isr_5:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 5
  .fin_isr5:
    hlt
  jmp .fin_isr5

isr_6:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 6
  .fin_isr6:
    hlt
  jmp .fin_isr6

;---------------------------------------------------------------------------------;
;  Excepción #NM "Device not available"                                           ;                                     
;    En este handler hago el resguardo y el restore de los registros SIMD         ;
;---------------------------------------------------------------------------------;
isr_7:
  clts                                         ; Limpio el flag de task switch de cr0
  pushad
  mov eax, [ULTIMO_CR3_SIMD]
  mov ebx, cr3
  cmp eax, ebx                                 ; Comparo el cr3 actual con el último almacenado
  je .fin_isr7                                 ; Si coinciden no hay necesidad de resguardar ni restorear nada
    ; Resguardo el contexto SIMD de la tarea anterior
    mov cr3, eax                               ; Cambio al espacio de ejecución de la tarea anterior  
    fxsave [___tasks_lineal_mmx_data_st]       ; Guardo los registros SIMD
    mov cr3, ebx                               ; Vuelvo al espacio de la tarea actual
  
    ; Recupero el contexto SIMD de la tarea
    fxrstor [___tasks_lineal_mmx_data_st]      ; Restore de los registros SIMD
  .fin_isr7:
  popad
iret

isr_8:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 8
  .fin_isr8:
    hlt
  jmp .fin_isr8

isr_9:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 9
  .fin_isr9:
    hlt
  jmp .fin_isr9

isr_10:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 10
  .fin_isr10:
    hlt
  jmp .fin_isr10

isr_11:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 11
  .fin_isr11:
    hlt
  jmp .fin_isr11

isr_12:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 12
  .fin_isr12:
    hlt
  jmp .fin_isr12

isr_13:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 13
  .fin_isr13:
    hlt
  jmp .fin_isr13

%ifdef PF_ENABLED                   ; Define para habilitar el handler de la #PF

; Excepción #PF Page Fault
PAGINA_DISPONIBLE:
  dd 0x10000000

isr_14:
  xchg bx,bx                        ; Magic breakpoint
  pop eax                           ; Saco de la pila el error code
  pushad

  mov ecx, eax                      ; Resguardo el error code
  and eax, 1
  cmp eax, 1                        ; Chequeo el bit de página no presente
  je .fin_isr14
    xor edx, edx                    ; Voy a guardar en edx el atributo que va a tener la nueva página

    ; Determino los atributos que va a tener la página en base al error code
    mov eax, ecx                    ; resguardo el error code
    and ecx, 2                      ; chequeo si el error fue por read (0) o por write (1)
    cmp ecx, 2
    je .errorPorWrite
    mov edx, 0x01                   ; U/S=0, R/W=0, P=1 
    jmp .chequeaErrorUS 
    .errorPorWrite:
    mov edx, 0x03                   ; U/S=0, R/W=1(writable), P=1

    .chequeaErrorUS:
    and eax, 4                      ; chequeo si el error fue en nivel supervisor o usuario
    cmp eax, 4
    je .errorPorUsr
    jmp .cargaNuevaPagina
    .errorPorUsr: 
    or edx, 4                       ; U/S=1 (modo usuario)

    .cargaNuevaPagina:
    mov esi, [PAGINA_DISPONIBLE]
    mov eax, cr2                    ; Obtengo la dirección de memoria que generó la excepción
  
    push ebp
    mov  ebp, esp
    push edx                        ; Atributos de la página
    push eax                        ; Dirección lineal
    push esi                        ; Dirección física de la página a agregar

    mov eax, cr3
    push eax                        ; Dirección de inicio del directorio de tablas de páginas
    call __set_ptree_entry_32
    leave
  
    add esi, 0x1000                 ; Actualizo la dirección de la próxima página disponible
    mov [PAGINA_DISPONIBLE], esi

  .fin_isr14:
  xchg bx, bx
  popad
iret

%else

  isr_14:
    xchg bx,bx           ; Magic breakpoint
    mov edx, 14
    .fin_isr14:
      hlt
  jmp .fin_isr14

%endif

isr_15:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 15
  .fin_isr15:
    hlt
  jmp .fin_isr15

isr_16:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 16
  .fin_isr16:
    hlt
  jmp .fin_isr16

isr_17:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 17
  .fin_isr17:
    hlt
  jmp .fin_isr17

isr_18:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 18
  .fin_isr18:
    hlt
  jmp .fin_isr18

isr_19:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 19
  iret
  .fin_isr19:
    hlt
  jmp .fin_isr19

isr_20:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 20
  iret
  .fin_isr20:
    hlt
  jmp .fin_isr20
;---------------------------------------------------------------------------------------------------;
;                                         ISR PIT                                                   ;
;---------------------------------------------------------------------------------------------------;

;--------------------------------------------------------------------------------; 
; Logica Scheduler                                                               ;
;   El loop completo del scheduler dura 200 mseg                                 ;
;   - La tarea1 se ejecuta cada 100 mseg (ventana de 20mseg para que se ejecute) ;
;   - La tarea2 se ejecuta cada 200 mseg (ventana de 20mseg para que se ejecute) ;
;--------------------------------------------------------------------------------;
;--------------------------------------------------------;
;Orden de ejecución de las tareas y tiempos de cada una: ;
;--------------------------------------------------------;
; Tarea | Tiempo switch tarea (mseg)    |
;  2    |   0                           |
;  0    |  20                           |
;  1    |  50                           |
;  0    |  70                           |
;  1    | 150                           |
;  0    | 170                           |
;  2    | 200 (vuelve a empezar el loop)|

ListaTareas:
  dd ___task2_cr3
  dd ___task0_cr3
  dd ___task1_cr3
  dd ___task0_cr3
  dd ___task1_cr3
  dd ___task0_cr3

; Los tiempos están expresados en decenas de mseg:
tiemposSwitchTareas:
  dd 0          ; empieza la tarea2
  dd 2          ; ventana de tiempo de 20ms, empieza tarea0
  dd 5          ; empieza la tarea1
  dd 7          ; ventana de tiempo de 20ms, empieza tarea0
  dd 15         ; empieza la tarea1
  dd 17         ; ventana de tiempo de 20ms, empieza tarea0

CONTADOR_TMR:
  dd 0

INDICE_LISTA_TAREAS:
  dd 0

ULTIMO_CR3_SIMD:
  dd ___task2_cr3                      ; La tarea2 es la primera que usa SIMD

isr_32:
  ; Resguardo registros
  pushad

  ; Resguardo selectores
  xor eax, eax
  mov ax, ds
  push eax 
  mov ax, fs
  push eax 
  mov ax, gs
  push eax 
  mov ax, es
  push eax 

  xor eax, eax
  xor esi, esi
  xor edi, edi

  ; Leo contador e índice de la lista de tareas a despachar
  mov eax, [CONTADOR_TMR]
  mov esi, [INDICE_LISTA_TAREAS]
  mov edi, [TAREA_FINALIZADA]

  ; Chequeo si hay una tarea que finalizó
  xor ecx, ecx
  mov ecx, cr3
  cmp ecx, edi                         ; Chequeo si la tarea actual finalizó
  jne .validaTiempoDeSwitch
    mov dword [TAREA_FINALIZADA], 0    ; Reseteo indicador de tarea finalizada
  jmp .procesaTaskSwitch

  .validaTiempoDeSwitch:
  ; Chequeo si se cumplió el tiempo para el cambio de tarea
  cmp eax, [tiemposSwitchTareas+esi*4]
  jne .fin_isr32

  .procesaTaskSwitch:

    ; Si es una tarea que usa SIMD guardo su cr3 en ULTIMO_CR3_SIMD
    mov ebx, cr3   
    cmp ebx, ___task0_cr3              ; si es la tarea0 (la única que no usa SIMD) no guardo el cr3
    je .finCargaUltimoSimdCr3
 
      mov [ULTIMO_CR3_SIMD], ebx       ; Guardo el cr3 de la última tarea que uso SIMD

      ; Inicializo el stack frame de la tarea a terminar (caso tarea1 y tarea2)
      mov dword [__L0_STACK_LINEAL_ADDR_END-4] , USR_RAM_DS_SEL             ; SS
      mov dword [__L0_STACK_LINEAL_ADDR_END-8] , __L3_STACK_LINEAL_ADDR_END ; ESP
      mov dword [__L0_STACK_LINEAL_ADDR_END-12], 0x202                      ; Cargo con un valor válido EFLAGS
      mov dword [__L0_STACK_LINEAL_ADDR_END-16], USR_RAM_CS_SEL_32          ; CS
      mov dword [__L0_STACK_LINEAL_ADDR_END-20], ___tasks_lineal_addr_st    ; EIP apunta al inicio de la tarea        
      mov dword [__L0_STACK_LINEAL_ADDR_END-24], 0                          ; registro de prop.gral. en 0
      mov dword [__L0_STACK_LINEAL_ADDR_END-28], 0                          ; registro de prop.gral. en 0
      mov dword [__L0_STACK_LINEAL_ADDR_END-32], 0                          ; registro de prop.gral. en 0
      mov dword [__L0_STACK_LINEAL_ADDR_END-36], 0                          ; registro de prop.gral. en 0
      mov dword [__L0_STACK_LINEAL_ADDR_END-40], 0                          ; registro de prop.gral. en 0
      mov dword [__L0_STACK_LINEAL_ADDR_END-44], 0                          ; registro de prop.gral. en 0
      mov dword [__L0_STACK_LINEAL_ADDR_END-48], 0                          ; registro de prop.gral. en 0
      mov dword [__L0_STACK_LINEAL_ADDR_END-52], 0                          ; registro de prop.gral. en 0
      mov dword [__L0_STACK_LINEAL_ADDR_END-56], USR_RAM_DS_SEL             ; DS
      mov dword [__L0_STACK_LINEAL_ADDR_END-60], USR_RAM_DS_SEL             ; FS
      mov dword [__L0_STACK_LINEAL_ADDR_END-64], USR_RAM_DS_SEL             ; GS
      mov dword [__L0_STACK_LINEAL_ADDR_END-68], USR_RAM_DS_SEL             ; ES
      jmp .cambiaCr3
    .finCargaUltimoSimdCr3:

      ; Inicializo el stack frame de la tarea a terminar (caso tarea0)
      mov dword [__L0_STACK_LINEAL_ADDR_END-4], 0x202                       ; Cargo con un valor válido EFLAGS
      mov dword [__L0_STACK_LINEAL_ADDR_END-8], RAM_CS_SEL_32               ; CS
      mov dword [__L0_STACK_LINEAL_ADDR_END-12], ___tasks_lineal_addr_st    ; EIP apunta al inicio de la tarea        
      mov dword [__L0_STACK_LINEAL_ADDR_END-16], 0                          ; registro de prop.gral. en 0
      mov dword [__L0_STACK_LINEAL_ADDR_END-20], 0                          ; registro de prop.gral. en 0
      mov dword [__L0_STACK_LINEAL_ADDR_END-24], 0                          ; registro de prop.gral. en 0
      mov dword [__L0_STACK_LINEAL_ADDR_END-28], 0                          ; registro de prop.gral. en 0
      mov dword [__L0_STACK_LINEAL_ADDR_END-32], 0                          ; registro de prop.gral. en 0
      mov dword [__L0_STACK_LINEAL_ADDR_END-36], 0                          ; registro de prop.gral. en 0
      mov dword [__L0_STACK_LINEAL_ADDR_END-40], 0                          ; registro de prop.gral. en 0
      mov dword [__L0_STACK_LINEAL_ADDR_END-44], 0                          ; registro de prop.gral. en 0
      mov dword [__L0_STACK_LINEAL_ADDR_END-48], RAM_DS_SEL                 ; DS
      mov dword [__L0_STACK_LINEAL_ADDR_END-52], RAM_DS_SEL                 ; FS
      mov dword [__L0_STACK_LINEAL_ADDR_END-56], RAM_DS_SEL                 ; GS
      mov dword [__L0_STACK_LINEAL_ADDR_END-60], RAM_DS_SEL                 ; ES

    .cambiaCr3:
    ; Cambio la traducción de páginas (cambio de tarea)
    xor ebx, ebx
    mov ebx, [ListaTareas+esi*4]
    and ebx, 0xfffff000
    mov cr3, ebx                       ; Cargo el cr3 de la nueva tarea
    inc esi
    
    ; Ubico el esp en la dirección que tiene que ir en base al cr3
    ; para que quede bien armado los datos del iret
    cmp ebx, ___task0_cr3              ; solamente la tarea0 corre con cpl==0
    je .cpl0Esp
    .cpl3Esp:
      mov ebp, 0x414fbb                ; Posición en la que debe estar el esp para las tareas con CPL=3
      jmp .ajustaEsp
    .cpl0Esp:
      mov ebp, 0x414fc3                ; Posición en la que debe estar el esp para las tareas con CPL=0
    .ajustaEsp:
      mov esp, ebp

    ; Seteo el flag de task switch
    mov edx, cr0
    or  edx, 0x00000008
    mov cr0, edx                       ; Seteo el bit de task switched (para el resguardo de los registros SIMD)
    
  .fin_isr32:
    inc eax
    cmp eax, 21                        ; Chequeo si el scheduler cumplio el ciclo completo
    jne .actualizaContador
    mov eax, 0                         ; Reseteo CONTADOR_TMR
    mov esi, 0                         ; Apunto el índice al inicio de la lista de tareas

    .actualizaContador:
    mov [CONTADOR_TMR], eax            ; Actualizo contador
    cmp esi, 6
    jne .actualizaIndice
    xor esi, esi                       ; Vuelvo el índice al inicio
    .actualizaIndice:
    mov [INDICE_LISTA_TAREAS], esi     ; Actualizo índice
    
  mov al, 0x20
  out 0x20, al                         ; Envío el end of interrupt al PIC

  ; Recupero selectores
  pop eax
  mov ds, ax
  pop eax
  mov fs, ax
  pop eax
  mov gs, ax
  pop eax
  mov es, ax

  ; Recupero registros
  popad
iret

;---------------------------------------------------------------------------------------------------;
;                                         ISR Teclado                                               ;
;---------------------------------------------------------------------------------------------------;
isr_33:
  pushad

  in al, 0x60                           ; Leemos el buffer de teclado

  mov bl, al
  and bl, 0x80                          ; Terminamos la isr si es un break code
  jnz .fin_isr33

  mov ecx, 17                           ; Cantidad de iteraciones para el loop que verifica si
                                        ; el caracter leído es hexadecimal

  cmp al, TECLA_ENTER                   ; Si se presiono la tecla "ENTER", copiamos el número de 64 bits a la tabla
  jne .loopVerificaCharHexValido
 
  .insertaNum64bitsEnTabla:
    ; Código de copiado del buffer de teclado a la tabla de dígitos

    ; Obtengo el índice del buffer de teclado que apunta a la última posición libre.
    ; Si el índice es 1 y la posición del digito es 1, significa que el buffer 
    ; está vacío -> no hago nada 
    xor eax, eax
    mov ecx, [INDICE_BUFFER_TECLADO]
    mov eax, [POSICION_DIGITO]
    cmp ecx, 1
    jne .bufferNoVacio
      cmp eax, 1
      je .fin_isr33

    .bufferNoVacio:

    cmp eax, 1
      je .procesaDigitos
    add ecx, 1                    ; Si la cantidad de digitos a insertar es impar
                                  ; hay que insertar un byte más 

    .procesaDigitos:
    ; Copio lo que está en el buffer de teclado a la tabla de dígitos
    mov edi, [INDICE_TABLA_DIGITOS]
    .copiaByteToTablaDigitos:
      mov eax, 0
      mov al, [BUFFER_TECLADO+ecx-1]
      mov [___tabla_de_digitos+edi], al
      inc edi 
    loop .copiaByteToTablaDigitos

    ; Si se ingresaron un numero impar de dígitos, tengo que shiftear lo
    ; el número resultante, 4 posiciones a la derecha
    xor eax, eax
    mov eax, [POSICION_DIGITO]
    cmp eax, 1
    je .noShiftDerecha
      mov edi, [INDICE_TABLA_DIGITOS]
      xor eax, eax
      xor ebx, ebx
      mov eax, [___tabla_de_digitos+edi]
      mov ebx, [___tabla_de_digitos+edi+4]

      ; Esto lo hago para shiftear el dígito menos significativo de los 4 bytes más significativos
      ; al dígito más significativo de los 4 bytes menos significativos
      shl ebx, 28
      shr eax, 4
      or  eax, ebx
      mov [___tabla_de_digitos+edi], eax    ; Guardo los 4 bytes menos significativos shifteados

      xor ebx, ebx
      mov ebx, [___tabla_de_digitos+edi+4]
      shr ebx, 4
      mov [___tabla_de_digitos+edi+4], ebx  ; Guardo los 4 bytes mas significativos shifteados
    .noShiftDerecha:

    ; Borro buffer de teclado
    mov eax, 0
    mov [BUFFER_TECLADO], eax             
    mov [BUFFER_TECLADO+4], eax
    mov [BUFFER_TECLADO+8], al

    ; Inicializo el índice de la posición del dígito
    mov byte [POSICION_DIGITO], 1

    ; Inicializo el índice del buffer de teclado
    mov eax, 1
    mov [INDICE_BUFFER_TECLADO], eax

    ; Actualizo el indice de la tabla de dígitos
    mov edi, [INDICE_TABLA_DIGITOS]
    add edi, 8
    cmp edi, 65536                      ; Vuelve el índice a cero si se llego a los 64kb
    jne .actualizaIndiceTablaDigitos
    xor edi, edi

    .actualizaIndiceTablaDigitos:
      mov [INDICE_TABLA_DIGITOS], edi
      jmp .fin_isr33

  .loopVerificaCharHexValido:           ; Si el caracter leído es hexadecimal lo copiamos al buffer
    cmp al, [TABLA_CHAR_VALIDOS+ecx-1]
    je .agregaDigitoAlBuffer
    loop .loopVerificaCharHexValido
    jmp .fin_isr33

  .agregaDigitoAlBuffer:
    mov edi, [INDICE_BUFFER_TECLADO]
    mov al, [TABLA_CHAR_TRADUCIDOS+ecx-1]
    mov edx, [POSICION_DIGITO]
    cmp edx, 1
    je .guardaDigitoPos1

    .guardaDigitoPos0:
      mov byte [POSICION_DIGITO], 1    ; Inicializo el índice de la posición del dígito
      mov ebx, [BUFFER_TECLADO+edi]
      or  eax, ebx
      mov [BUFFER_TECLADO+edi], al

    ; Incrementamos el índice
    inc edi                              
    cmp edi, 9
    ; Si se llego al final del buffer, apunto el índice al principio
    jne .actualizaIndice
    mov edi, 1

  .actualizaIndice:
    mov [INDICE_BUFFER_TECLADO], edi
    jmp .fin_isr33

  .guardaDigitoPos1:
    shl al, 4
    mov [BUFFER_TECLADO+edi], al
    mov byte [POSICION_DIGITO], 0

  .fin_isr33:
    mov al, 0x20
    out 0x20, al                         ; Envío el end of interrupt al PIC
  popad
iret

TECLA_ENTER equ 0x1c

ALIGN 16
TABLA_CHAR_VALIDOS:  ; Tabla que contiene los scan codes válidos a insertar
  db 0x0b            ; '0'
  db 0x02            ; '1'
  db 0x03            ; '2'
  db 0x04            ; '3'
  db 0x05            ; '4'
  db 0x06            ; '5'
  db 0x07            ; '6'
  db 0x08            ; '7'
  db 0x09            ; '8'
  db 0x0a            ; '9'
  db 0x1e            ; 'a'
  db 0x30            ; 'b'
  db 0x2e            ; 'c'
  db 0x20            ; 'd'
  db 0x12            ; 'e'
  db 0x21            ; 'f'

TABLA_CHAR_TRADUCIDOS:  ; Tabla que contiene los scan codes válidos a insertar
  db 0x00               ; '0'
  db 0x01               ; '1'
  db 0x02               ; '2'
  db 0x03               ; '3'
  db 0x04               ; '4'
  db 0x05               ; '5'
  db 0x06               ; '6'
  db 0x07               ; '7'
  db 0x08               ; '8'
  db 0x09               ; '9'
  db 0x0a               ; 'a'
  db 0x0b               ; 'b'
  db 0x0c               ; 'c'
  db 0x0d               ; 'd'
  db 0x0e               ; 'e'
  db 0x0f               ; 'f'

BUFFER_TECLADO:
  db 0
  db 0
  db 0
  db 0
  db 0
  db 0
  db 0
  db 0
  db 0
  
INDICE_TABLA_DIGITOS:
  dd 0

INDICE_BUFFER_TECLADO:
  dd 1

POSICION_DIGITO:
  db 1

TABLA_ISRs:          ; En esta tabla guardo las direcciones de las ISRs
  dd isr_0
  dd isr_1
  dd isr_2
  dd isr_3
  dd isr_4
  dd isr_5
  dd isr_6
  dd isr_7
  dd isr_8
  dd isr_9
  dd isr_10
  dd isr_11
  dd isr_12
  dd isr_13
  dd isr_14
  dd isr_15
  dd isr_16
  dd isr_17
  dd isr_18
  dd isr_19
  dd isr_20
  dd isr_32
  dd isr_33

