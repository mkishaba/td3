/* 
  \file  init32.c
  \version 01.00
  \brief Definicion de funciones de inicializacion del procesador en 32b

  \author Christian Nigri <cnigri@utn.edu.ar>
  \date    18/04/2017
*/
#include "../inc/sys_types.h"
#include "../inc/defs.h"

/******************************************************************************
*
* @fn         __fast_memcpy
*
* @brief      Esta funcion copia length double words desde dst a src. 
*             No se raliza validacion de las regiones de memoria.
*             En caso de exito retorna EXITO o ERROR_DEFECTO en cualquier otra 
*             circunstancia
*
* @param [in] src puntero tipo double word. Especifica la direccion de origen.
*
* @param [in] dst puntero tipo double word. Especifica la direccion de destino.
*
* @param [in] length tipo double word. Especifica el numero de double wors a copiar.
*
* @return     tipo byte indicando si falla o no.
*
******************************************************************************/
__attribute__(( section(".init32"))) byte __fast_memcpy(const dword *src, dword *dst, dword length)
{

   byte status = ERROR_DEFECTO;

   if(length > 0)
   {
      
      while(length)
      {
         length--;
         *dst++ = *src++;
      }
      status = EXITO;   
   }

   return(status);
}

/******************************************************************************
*
* @fn         __set_ptree_entry_32
*
* @brief      Esta funcion inicializa las entradas del arbol de paginacion sin
*             PAE para la direccion lineal address_lin, asociada con la
*             direccion fisica address_phy, correspondientes a la base del arbol
*             de paginacion ptree_base, con los atributos page_attr.
*
* @param [in] ptree_base tipo double word. Especifica la direccion base del
*              arbol de paginacion.
*
* @param [in] addr_phy tipo double word. Especifica la direccion fisica de la
*             pagina.
*
* @param [in] addr_lin tipo double word. Especifica la direccion lineal de la
*             pagina.
*
* @param [in] page_attr tipo word. Especifica los atributos de las entradas
*             correspondientes a la nueva pagina.
*
* @return     tipo byte indicando si falla o no.
*
*******************************************************************************/
__attribute__(( section(".kernel32")))
void __set_ptree_entry_32( dword const ptree_base, dword addr_phy, dword addr_lin, dword page_attr) 
{
  dword *ptree_base_ptr = (dword *) ptree_base;
  // Cargo entrada en el directorio de tablas de página
  *(ptree_base_ptr + PDT_ENTRY) = ((ptree_base + (PDT_ENTRY+1) * 4096) & PAGE_ADDR_MASK) | page_attr; 
  // Cargo entrada en la tabla de páginas
  *((dword *)((byte *)ptree_base_ptr + (PDT_ENTRY+1) * 4096 + PAGE_TABLE_OFFSET * 4)) = (addr_phy & PAGE_ADDR_MASK) | page_attr;
}

// MAPA DE MEMORIA             DIR.FIS.                 DIR.LIN
/*
 ISR                          00000000h                00000000h
 Tablas de sistema            00100000h                00100000h
 Tablas de paginación         00110000h                00110000h
 Núcleo                       00300000h                00300000h
 TEXT Tarea 0                 00301000h                00410000h
 BSS Tarea 0                  00302000h                00411000h
 DATA Tarea 0                 00303000h                00412000h
 Tabla de dígitos             00310000h                00310000h
 TEXT Tarea 1                 00321000h                00410000h
 BSS Tarea 1                  00322000h                00411000h
 DATA Tarea 1                 00323000h                00412000h
 TEXT Tarea 2                 00331000h                00410000h
 BSS Tarea 2                  00332000h                00411000h
 DATA Tarea 2                 00333000h                00412000h
 Datos                        003E0000h                003E0000h
 Pila Núcleo Tarea 0          1FFF8000h                00414000h
 Pila Núcleo Tarea 2          1FFF9000h                00414000h
 Pila Núcleo Tarea 1          1FFFA000h                00414000h
 Pila Núcleo                  1FFFB000h                1FFFB000h
 Pila Usuario Tarea 0         1FFFC000h                00413000h
 Pila Usuario Tarea 2         1FFFD000h                00413000h
 Pila Usuario Tarea 1         1FFFE000h                00413000h
 Secuencia inicialización ROM FFFF0000h                FFFFF000h
 Vector de reset              FFFFFFF0h                FFFFFFF0h
*/
extern dword ___task0_cr3;
extern dword ___task1_cr3;
extern dword ___task2_cr3;
extern dword ___task0_vma_st;
extern dword ___task1_vma_st;
extern dword ___task2_vma_st;
extern dword ___task0_bss_st;
extern dword ___task1_bss_st;
extern dword ___task2_bss_st;
extern dword ___task0_data_st;
extern dword ___task1_data_st;
extern dword ___task2_data_st;
extern dword __T0_L0_STACK_START;
extern dword __T1_L0_STACK_START;
extern dword __T2_L0_STACK_START;
extern dword __T0_L3_STACK_START;
extern dword __T1_L3_STACK_START;
extern dword __T2_L3_STACK_START;
extern dword ___tasks_lineal_addr_st;
extern dword ___tasks_lineal_bss_st;
extern dword ___tasks_lineal_data_st;
extern dword ___tasks_lineal_buffer_tabla;
extern dword __L3_STACK_LINEAL_ADDR;
extern dword __L0_STACK_LINEAL_ADDR;
extern dword ___tabla_de_digitos;
extern dword ___task1_buffer_tabla;
extern dword ___task2_buffer_tabla;
extern dword ___isr_vma_st;
extern dword ___tablas_de_sistema_vma_st;
extern dword ___kernel_vma_st;
extern dword __datos_no_inicializados;
extern dword __STACK_START_32;

//*************************************************************************************
//*  Función __set_tasks_paging_tables                                                *
//*  Genera las tablas de paginación para task0, task1 y task2                        *
//*************************************************************************************
__attribute__(( section(".kernel32")))
void __set_tasks_paging_tables(void) {
  // ISR
  __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &___isr_vma_st , (dword) &___isr_vma_st, 0x03);
  __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &___isr_vma_st , (dword) &___isr_vma_st, 0x03);
  __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &___isr_vma_st , (dword) &___isr_vma_st, 0x03);

  // Tablas de sistema
  __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &___tablas_de_sistema_vma_st, (dword) &___tablas_de_sistema_vma_st, 0x03);
  __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &___tablas_de_sistema_vma_st, (dword) &___tablas_de_sistema_vma_st, 0x03);
  __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &___tablas_de_sistema_vma_st, (dword) &___tablas_de_sistema_vma_st, 0x03);

  // Tablas de paginación
  __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &___task0_cr3, (dword) &___task0_cr3, 0x03);
  __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &___task1_cr3, (dword) &___task1_cr3, 0x03);
  __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &___task2_cr3, (dword) &___task2_cr3, 0x03);

  // Por si se quiere probar el handler de #PF (genero la entrada para la primer tabla de página)
  __set_ptree_entry_32( (dword) &___task0_cr3, 0x111000, 0x111000, 0x03);
  __set_ptree_entry_32( (dword) &___task1_cr3, 0x193000, 0x193000, 0x03);
  __set_ptree_entry_32( (dword) &___task2_cr3, 0x215000, 0x215000, 0x03);

  // Núcleo
  __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &___kernel_vma_st, (dword) &___kernel_vma_st, 0x03);
  __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &___kernel_vma_st, (dword) &___kernel_vma_st, 0x03);
  __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &___kernel_vma_st, (dword) &___kernel_vma_st, 0x03);

  // Tabla de dígitos
  __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &___tabla_de_digitos, (dword) &___tabla_de_digitos, 0x03);
  __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &___tabla_de_digitos, (dword) &___tabla_de_digitos, 0x03);
  __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &___tabla_de_digitos, (dword) &___tabla_de_digitos, 0x03);

  // Datos
  __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &__datos_no_inicializados, (dword) &__datos_no_inicializados, 0x03);
  __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &__datos_no_inicializados, (dword) &__datos_no_inicializados, 0x03);
  __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &__datos_no_inicializados, (dword) &__datos_no_inicializados, 0x03);

  // Pila núcleo
  __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &__STACK_START_32, (dword) &__STACK_START_32, 0x03);
  __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &__STACK_START_32, (dword) &__STACK_START_32, 0x03);
  __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &__STACK_START_32, (dword) &__STACK_START_32, 0x03);

  // Cargo las tablas de paginación específicas de cada tarea
    // sección .text
    __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &___task0_vma_st, (dword) &___tasks_lineal_addr_st, 0x03);
    __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &___task1_vma_st, (dword) &___tasks_lineal_addr_st, 0x07);
    __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &___task2_vma_st, (dword) &___tasks_lineal_addr_st, 0x07);
    // sección .bss
    __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &___task0_bss_st, (dword) &___tasks_lineal_bss_st, 0x03);
    __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &___task1_bss_st, (dword) &___tasks_lineal_bss_st, 0x07);
    __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &___task2_bss_st, (dword) &___tasks_lineal_bss_st, 0x07);
    // sección .data
    __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &___task0_data_st, (dword) &___tasks_lineal_data_st, 0x03);
    __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &___task1_data_st, (dword) &___tasks_lineal_data_st, 0x07);
    __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &___task2_data_st, (dword) &___tasks_lineal_data_st, 0x07);
 
    // Pagina para el buffer de la tabla de dígitos
    __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &___task1_buffer_tabla, (dword) &___tasks_lineal_buffer_tabla, 0x07);
    __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &___task2_buffer_tabla, (dword) &___tasks_lineal_buffer_tabla, 0x07);

    // pila nivel 0
    __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &__T0_L0_STACK_START, (dword) &__L0_STACK_LINEAL_ADDR, 0x03);
    __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &__T1_L0_STACK_START, (dword) &__L0_STACK_LINEAL_ADDR, 0x07);
    __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &__T2_L0_STACK_START, (dword) &__L0_STACK_LINEAL_ADDR, 0x07);
    // pila nivel 3
    __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &__T0_L3_STACK_START, (dword) &__L3_STACK_LINEAL_ADDR, 0x03);
    __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &__T1_L3_STACK_START, (dword) &__L3_STACK_LINEAL_ADDR, 0x07);
    __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &__T2_L3_STACK_START, (dword) &__L3_STACK_LINEAL_ADDR, 0x07);
}
