GLOBAL ROM_CS_SEL_32
GLOBAL ROM_DS_SEL
GLOBAL ROM_GDT_LENGTH
GLOBAL RAM_CS_SEL_32
GLOBAL USR_RAM_CS_SEL_32
GLOBAL RAM_DS_SEL
GLOBAL USR_RAM_DS_SEL
GLOBAL RAM_LDT_SEL
GLOBAL TASK0_SEL
GLOBAL HLT_SYS_CALL_SEL
GLOBAL READ_SYS_CALL_SEL
GLOBAL INDEX_SYS_CALL_SEL
GLOBAL RAM_GDT
GLOBAL rom_gdtr
GLOBAL ram_gdtr
GLOBAL rom_idtr

EXTERN ___inicio_idt_ram

SECTION .sys_tables progbits alloc noexec nowrite
ALIGN 8

; Tabla GDT en RAM
RAM_GDT:
NULL_SEL          equ $-RAM_GDT
   dq 0x0
RAM_DS_SEL        equ $-RAM_GDT
   dw 0xffff                     ; Límite bits 0-15
   dw 0x0000                     ; Base bits 0-15
   db 0x00                       ; Base bits 16-23
   db 10010010b                  ; P=1(Seg. presente), DPL=0, S=1, Bit11=0(datos), W=1(writable)
   db 11001111b                  ; G=1(limitex4K), D/B=1(32bits), L=0, AVL=0 / Límite bits 19-16
   db 0                          ; Base bits 31-24
RAM_CS_SEL_32     equ $-RAM_GDT
   dw 0xffff                     ; Límite bits 0-15
   dw 0x0000                     ; Base bits 0-15
   db 0x00                       ; Base bits 16-23
   db 10011010b                  ; P=1(Seg. presente), DPL=0, S=1, Bit11=1(código), R=1(readable)
   db 11001111b                  ; G=1(limitex4K), D/B=1(32bits), L=0, AVL=0 / Límite bits 19-16
   db 0                          ; Base bits 31-24
USR_RAM_DS_SEL    equ $-RAM_GDT+3
   dw 0xffff                     ; Límite bits 0-15
   dw 0x0000                     ; Base bits 0-15
   db 0x00                       ; Base bits 16-23
   db 11110010b                  ; P=1(Seg. presente), DPL=11, S=1, Bit11=0(datos), W=1(writable)
   db 11001111b                  ; G=1(limitex4K), D/B=1(32bits), L=0, AVL=0 / Límite bits 19-16
   db 0                          ; Base bits 31-24
USR_RAM_CS_SEL_32 equ $-RAM_GDT+3
   dw 0xffff                     ; Límite bits 0-15
   dw 0x0000                     ; Base bits 0-15
   db 0x00                       ; Base bits 16-23
   db 11111110b                  ; P=1(Seg. presente), DPL=11, S=1, Bit11=1(código), R=1(readable)
   db 11001111b                  ; G=1(limitex4K), D/B=1(32bits), L=0, AVL=0 / Límite bits 19-16
   db 0                          ; Base bits 31-24
RAM_LDT_SEL       equ $-RAM_GDT
   dw 0xffff                     ; Límite bits 0-15
   dw 0x0000                     ; Base bits 0-15
   db 0x00                       ; Base bits 16-23
   db 10000010b                  ; P=1(Seg. presente), DPL=0, S=0(descriptor de sistema), Tipo:LDT(0010b)
   db 11001111b                  ; G=1(limitex4K), D/B=1(32bits), L=0, AVL=0 / Límite bits 19-16
   db 0                          ; Base bits 31-24
HLT_SYS_CALL_SEL  equ $-RAM_GDT  ; Reservo espacio para generar el descriptor de syscall
   dq 0
READ_SYS_CALL_SEL equ $-RAM_GDT  ; Reservo espacio para generar el descriptor de syscall
   dq 0
INDEX_SYS_CALL_SEL equ $-RAM_GDT ; Reservo espacio para generar el descriptor de syscall
   dq 0
TASK0_SEL         equ $-RAM_GDT
   dq 0                          ; Reservo espacio para generar el descriptor de la tarea0
RAM_GDT_LENGTH    equ $-RAM_GDT

;Registros idtr y gdtr
ram_gdtr:
   dw RAM_GDT_LENGTH-1        ; Se resta el selector nulo
   dd RAM_GDT

SECTION .rom_sys_tables progbits alloc noexec nowrite

; Tabla GDT en ROM
ROM_GDT:
ROM_NULL_SEL    equ $-ROM_GDT
   dq 0x0
ROM_DS_SEL      equ $-ROM_GDT
   dw 0xffff                  ; Límite bits 0-15
   dw 0x0000                  ; Base bits 0-15
   db 0x00                    ; Base bits 16-23
   db 10010010b               ; P=1(Seg. presente), DPL=0, S=1, Bit11=0(datos), W=1(writable)
   db 11001111b               ; G=1(limitex4K), D/B=1(32bits), L=0, AVL=0 / Límite bits 19-16
   db 0                       ; Base bits 31-24
ROM_CS_SEL_32   equ $-ROM_GDT
   dw 0xffff                  ; Límite bits 0-15
   dw 0x0000                  ; Base bits 0-15
   db 0x00                    ; Base bits 16-23
   db 10011010b               ; P=1(Seg. presente), DPL=0, S=1, Bit11=1(código), R=1(readable)
   db 11001111b               ; G=1(limitex4K), D/B=1(32bits), L=0, AVL=0 / Límite bits 19-16
   db 0                       ; Base bits 31-24
ROM_GDT_LENGTH  equ $-ROM_GDT

;Zona de datos para cargar idtr y gdtr
rom_gdtr:
   dw ROM_GDT_LENGTH-1        ; Se resta el selector nulo
   dd ROM_GDT

; La IDT se va generar en forma dinámica en el código del kernel
rom_idtr:
  dw 256*8                    ; Largo de la IDT (256 tipos de interrupciones)
  dd ___inicio_idt_ram        ; Dirección de comienzo de la IDT
