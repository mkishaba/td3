/* 
 PDT_ENTRY
 Macro para obtener la entrada del directorio de 
 tablas de página a partir de la dirección lineal
*/
#define PDT_ENTRY (addr_lin >> 22)
#define PAGE_TABLE_OFFSET ((addr_lin >> 12) & 0x000003ff)

/*
 PDT_ENTRY_ATTR
 Atributos descriptor del directorio de tablas de páginas:
 P   (present)              = 1 (página presente)
 R/W (readable/writeable)   = 1 (writeable)
 U/S (user/kernel)          = 0 (modo kernel)
 PWT (page write through)   = 0 (deshabilitado)
 PCD (page cache disabled)  = 1 (sin cache)
 A   (accessed)             = 0 (página no accedida)
 IGN (ignored)              = 0
 PS  (page size)            = 0 (páginas de 4kbytes)
*/
#define PDT_ENTRY_ATTR 0x00000013
#define PAGE_ATTR      0x00000013

#define PAGE_ADDR_MASK (0xfffff000)
