GLOBAL codigoTareaIdle

SECTION .task0 progbits

; Tarea idle
codigoTareaIdle:
  xchg bx, bx
  sti
  .haltLoop:
    hlt
  jmp .haltLoop
