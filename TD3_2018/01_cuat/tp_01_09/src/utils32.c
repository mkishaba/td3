/* 
  \file  init32.c
  \version 01.00
  \brief Definicion de funciones de inicializacion del procesador en 32b

  \author Christian Nigri <cnigri@utn.edu.ar>
  \date    18/04/2017
*/
#include "../inc/sys_types.h"
#include "../inc/defs.h"

/******************************************************************************
*
* @fn         __fast_memcpy
*
* @brief      Esta funcion copia length double words desde dst a src. 
*             No se raliza validacion de las regiones de memoria.
*             En caso de exito retorna EXITO o ERROR_DEFECTO en cualquier otra 
*             circunstancia
*
* @param [in] src puntero tipo double word. Especifica la direccion de origen.
*
* @param [in] dst puntero tipo double word. Especifica la direccion de destino.
*
* @param [in] length tipo double word. Especifica el numero de double wors a copiar.
*
* @return     tipo byte indicando si falla o no.
*
******************************************************************************/
__attribute__(( section(".init32"))) byte __fast_memcpy(const dword *src, dword *dst, dword length)
{

   byte status = ERROR_DEFECTO;

   if(length > 0)
   {
      
      while(length)
      {
         length--;
         *dst++ = *src++;
      }
      status = EXITO;   
   }

   return(status);
}

/******************************************************************************
*
* @fn         __set_ptree_entry_32
*
* @brief      Esta funcion inicializa las entradas del arbol de paginacion sin
*             PAE para la direccion lineal address_lin, asociada con la
*             direccion fisica address_phy, correspondientes a la base del arbol
*             de paginacion ptree_base, con los atributos page_attr.
*
* @param [in] ptree_base tipo double word. Especifica la direccion base del
*              arbol de paginacion.
*
* @param [in] addr_phy tipo double word. Especifica la direccion fisica de la
*             pagina.
*
* @param [in] addr_lin tipo double word. Especifica la direccion lineal de la
*             pagina.
*
* @param [in] page_attr tipo word. Especifica los atributos de las entradas
*             correspondientes a la nueva pagina.
*
* @return     tipo byte indicando si falla o no.
*
*******************************************************************************/
__attribute__(( section(".init32"))) void __set_ptree_entry_32( dword const ptree_base,
                                                                dword addr_phy,
                                                                dword addr_lin,
                                                                word page_attr)
{
  dword *ptree_base_ptr = (dword *) ptree_base;

  // Cargo entrada en el directorio de tablas de página
  *(ptree_base_ptr + PDT_ENTRY) = ((ptree_base + (PDT_ENTRY+1) * 4096) & PAGE_ADDR_MASK) | PDT_ENTRY_ATTR; 

  // Cargo entrada en la tabla de páginas
  *((dword *)((byte *)ptree_base_ptr + (PDT_ENTRY+1) * 4096 + PAGE_TABLE_OFFSET * 4)) = (addr_phy & PAGE_ADDR_MASK) | page_attr;
}

__attribute__(( section(".init32"))) void __set_paging_tables( dword const ptree_base, dword max_addr )
{
  dword i,j;
  dword addr_phy=0;
  dword addr_lin=0;

  dword pageTableDirectoryEntries = ((max_addr & PAGE_ADDR_MASK) / 4096) / 1024;  // Obtengo la última entrada en la tabla de directorio
  dword remainEntries = ((max_addr & PAGE_ADDR_MASK) / 4096) % 1024;              // Obtengo la última entrada de la última tabla de páginas

  // Cargo entradas para tablas de páginas completas (1024 entradas)
  for (i=0; i<pageTableDirectoryEntries; i++) {
    for (j=0; j<1024; j++) {
      addr_phy = i*4*1024*1024+j*4*1024;
      addr_lin = ((dword)i<<22)+((dword)j<<12);
      __set_ptree_entry_32(ptree_base, addr_phy, addr_lin, PAGE_ATTR);
    }
  }

  // Cargo las entradas restantes de la última tabla de páginas
  for (j=0; j<remainEntries; j++) {
    addr_phy = i*4*1024*1024+j*4*1024;
    addr_lin = ((dword)i<<22)+((dword)j<<12);
    __set_ptree_entry_32(ptree_base, addr_phy, addr_lin, PAGE_ATTR);
  }
}
