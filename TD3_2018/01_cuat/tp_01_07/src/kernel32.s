SECTION  .kernel32 progbits

GLOBAL kernel32_init
GLOBAL TABLA_ISRs

EXTERN main
EXTERN CS_SEL_32
EXTERN ___tablas_de_sistema
EXTERN IDTR

USE32
kernel32_init: 
  xchg bx, bx         ; Magic breakpoint

  call initPIC        ; Inicialización del controlador de interrupciones
  call cargaIDT       ; Cargo la IDT en RAM

  jmp dword CS_SEL_32:main   
  .guard:
    hlt
    jmp .guard

;-----------------------;
;        initPIC        ;
;-----------------------;
initPIC:
  pushad
  ;Inicialización PICs
  mov bx, 0x2028      ; Base de los PICS (0x20 PIC1 0x28 PIC2)

  ;Inicialización PIC Nº1 (Master)
  mov al, 0x11        ; ICW1
  out 0x20,al         ; IRQs activas x flanco/cascada/ICW4 requerida
                      
  mov al, bh          ; ICW2
  out 0x21, al        ; El PIC Nº1 arranca en INT tipo (BH)
                      
  mov al, 0x4         ; ICW3
  out 21h, al         ; PIC1 Master, Slave ingresa Int.x IRQ2 (ver PDF pag. 90)
                      
  mov al, 0x1         ; ICW4
  out 0x21, al        ; Modo 8086/Non buffered mode/Not special fully nested mode
                      
  mov al, 0xff        ; Antes de inicializar el PIC Nº2, deshabilitamos 
  out 0x21, al        ; las Interrupciones del PIC1
                      ; Todas las interrupciones enmascaradas

  ;Inicialización PIC Nº2 (Slave)
  mov al, 11h         ;ICW1
  out 0xA0, al        ;IRQs activas x flanco/cascada/ICW4 requerida
                      
  mov al, bl          ;ICW2
  out 0xA1, al        ;El PIC Nº2 arranca en INT tipo (BL)
                      
  mov al, 0x02        ;ICW3
  out 0xA1, al        ;PIC2 Slave, ingresa Int x IRQ2
                      
  mov al, 0x1         ;ICW4
  out 0xA1, al        ;Modo 8086
                      
  mov al, 0xff        ;Enmascaramos el resto de las Interrupciones 
  out 0xA1, al        ;(las del PIC Nº2)

  popad
ret

;---------------------;
; Creación de la IDT  ;
;---------------------;
cargaIDT:
  pushad
  mov ecx, 20
  xor edi, edi
  
  comienzoLoopCargaIDT:
    mov eax, [TABLA_ISRs+edi*4]                              ; Obtengo la dirección de la ISR
    mov byte [___tablas_de_sistema+edi*8]  , al              ; Offset ISR parte baja (little endian)
    mov byte [___tablas_de_sistema+edi*8+1], ah              ; Offset ISR parte baja (little endian)
    mov word [___tablas_de_sistema+edi*8+2], CS_SEL_32       ; Selector de segmento de código
    mov byte [___tablas_de_sistema+edi*8+4], 0               ; Selector de segmento de código
    mov byte [___tablas_de_sistema+edi*8+5], 0x8e            ; presente=1, dpl=0, s=0, tipo=interrupt gate
    shr eax, 16                                              ; Obtengo la parte alta del offset
    mov byte [___tablas_de_sistema+edi*8+6], al              ; Offset ISR parte alta (little endian)
    mov byte [___tablas_de_sistema+edi*8+7], ah              ; Offset ISR parte alta (little endian)
    inc edi                                                  ; Incremento el índice
  loop comienzoLoopCargaIDT
  lidt [IDTR] 
  popad
ret

;---------------------------------------------------;
; Rutinas de atención de interrupciones/excepciones ;
;---------------------------------------------------;
isr_0:
  mov eax, 0
  mov ebx, [0xffffff00] ; para generar un PE en el caso de querer generar una doble falta
  .fin_isr0:
    hlt
  jmp .fin_isr0

isr_1:
  mov eax, 1
  .fin_isr1:
    hlt
  jmp .fin_isr1

isr_2:
  mov eax, 2
  .fin_isr2:
    hlt
  jmp .fin_isr2

isr_3:
  mov eax, 3
  .fin_isr3:
    hlt
  jmp .fin_isr3

isr_4:
  mov eax, 4
  .fin_isr4:
    hlt
  jmp .fin_isr4

isr_5:
  mov eax, 5
  .fin_isr5:
    hlt
  jmp .fin_isr5

isr_6:
  mov eax, 6
  .fin_isr6:
    hlt
  jmp .fin_isr6

isr_7:
  mov eax, 7
  .fin_isr7:
    hlt
  jmp .fin_isr7

isr_8:
  mov eax, 8
  .fin_isr8:
    hlt
  jmp .fin_isr8

isr_9:
  mov eax, 9
  .fin_isr9:
    hlt
  jmp .fin_isr9

isr_10:
  mov eax, 10
  .fin_isr10:
    hlt
  jmp .fin_isr10

isr_11:
  mov eax, 11
  .fin_isr11:
    hlt
  jmp .fin_isr11

isr_12:
  mov eax, 12
  .fin_isr12:
    hlt
  jmp .fin_isr12

isr_13:
  mov eax, 13
  .fin_isr13:
    hlt
  jmp .fin_isr13

isr_14:
  mov eax, 14
  .fin_isr14:
    hlt
  jmp .fin_isr14

isr_15:
  mov eax, 15
  .fin_isr15:
    hlt
  jmp .fin_isr15

isr_16:
  mov eax, 16
  .fin_isr16:
    hlt
  jmp .fin_isr16

isr_17:
  mov eax, 17
  .fin_isr17:
    hlt
  jmp .fin_isr17

isr_18:
  mov eax, 18
  .fin_isr18:
    hlt
  jmp .fin_isr18

isr_19:
  mov eax, 19
  .fin_isr19:
    hlt
  jmp .fin_isr19

TABLA_ISRs:             ; En esta tabla guardo las direcciones de las ISRs
  dd isr_0
  dd isr_1
  dd isr_2
  dd isr_3
  dd isr_4
  dd isr_5
  dd isr_6
  dd isr_7
  dd isr_8
  dd isr_9
  dd isr_10
  dd isr_11
  dd isr_12
  dd isr_13
  dd isr_14
  dd isr_15
  dd isr_16
  dd isr_17
  dd isr_18
  dd isr_19
