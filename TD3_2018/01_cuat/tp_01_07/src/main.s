SECTION .main progbits
GLOBAL main
EXTERN ___tabla_de_digitos
EXTERN ___fin_tabla_de_digitos
EXTERN ___tablas_de_sistema

USE32
main:
  mov edi, ___tabla_de_digitos           ; Inicializamos el índice a la dirección de inicio de la tabla
 
  ; Bucle polling de teclado
  .kybrd_poll:
    xor   eax, eax
    in    al,  0x64                      ; Lee el puerto 0x64 (status register del 8042 (controlador de teclado))
    bt    eax, 0x00                      ; Verificamos que haya algo que leer (bit0 = 1 => output buffer full)
  jnc  .kybrd_poll                       ; Vuelve a ejecutar el bucle si no hay nada que leer del buffer de teclado

    in    al, 0x60                       ; Leemos el buffer de teclado
    cmp   al, 0x1f                       ; Chequeamos si se presiono la tecla 's'
  je .fin                                ; Terminamos el programa si se presiono la tecla 's'

    cmp   al, 0x15                       ; Chequeamos si se presiono la tecla 'y'
    je .genera_DE                        ; Generamos excepción por error de división 

    cmp   al, 0x16                       ; Chequeamos si se presiono la tecla 'u'
    je .genera_UD                        ; Generamos

    cmp   al, 0x17                       ; Chequeamos si se presiono la tecla 'i'
    je .genera_DF                        ; generamos

    cmp   al, 0x18                       ; Chequeamos si se presiono la tecla 'o'
    je .genera_GP                        ; Generamos excepción de general proteccion

    cmp   al, 0x19                       ; Chequeamos si se presiono la tecla 'p'
    je .genera_PF                        ; Generamos excepción por page faut

    mov bl, al
    and bl, 0x80                         ; Vuelvo al inicio del bucle si es un break code
  jnz .kybrd_poll

    xchg  bx, bx                         ; Magic breakpoint (se presiono una tecla)
    mov   [edi], al                      ; Si no se presiono la tecla 's', guardamos en la tabla la tecla presionada
    inc   edi                            ; Incrementamos el índice (overflow cuando llega a los 64K, buffer circular)
    cmp   edi, ___fin_tabla_de_digitos   ; Verificamos si el índice ya llego a los 64k de la tabla
    jne   .kybrd_poll                    ; Si no se llego al final de la tabla se continua con el polling
    mov   edi, ___tabla_de_digitos       ; Inicializamos el índice a la dirección de inicio de la tabla
  jmp .kybrd_poll

  .genera_DE:
    xchg  bx, bx                         ; Magic breakpoint (se presiono una tecla)
    mov eax, 1                           ; dividendo = 1
    xor ecx, ecx                         ; divisor = 0
    div ecx                              ; 1/0
  je .fin

  .genera_UD:
    xchg   bx, bx                        ; Magic breakpoint (se presiono una tecla)
    movaps xmm1, xmm0                    ; Pruebo una instrucción de SIMD para generar un invalid opcode
  je .fin

  .genera_DF:                               ; Para generar doble falta genero un DE con un Segmento no presente 
    xchg   bx, bx                           ; Magic breakpoint (se presiono una tecla)
    mov byte [___tablas_de_sistema+5], 0x0e ; Modifico el descriptor 0 de la IDT con el atributo segmento no presente
    mov eax, 1                              ; dividendo = 1
    xor ecx, ecx                            ; divisor = 0
    div ecx                                 ; 1/0
  je .fin

  .genera_GP:
    xchg  bx, bx                         ; Magic breakpoint (se presiono una tecla)
    xor eax, eax                         ; Cargo ds con el descriptor nulo para que me de
    mov ds, eax                          ; un error de protección (al querer acceder a un dato)
    mov eax, [0xffffff00]
  je .fin
 
  .genera_PF:
    xchg  bx, bx                         ; Magic breakpoint (se presiono una tecla)
    int 14                               ; La excepción de page fault la genero por ahora mediante INT
  je .fin

  ; Finalización del programa
  .fin:
    hlt
    jmp .fin
