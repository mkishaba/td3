GLOBAL inicioTarea1

EXTERN TASK0_SEL_K_MODE
EXTERN INDICE_TABLA_DIGITOS
EXTERN ___tabla_de_digitos
EXTERN ___tasks_lineal_bss_st

SECTION .task2 progbits

;--------------------------------------------------------------------;
;                              Task 2                                ;
; Suma todos los números de 8 bytes guardados en la tabla de dígitos ;
;--------------------------------------------------------------------;
inicioTarea2:
  ; Obtengo el valor del índice de la tabla de dígitos 
  mov eax, [INDICE_TABLA_DIGITOS]
  cmp eax, 0
  je .fin_suma

;  xchg bx, bx
  ; A partir del índice de la tabla de dígitos obtengo 
  ; la cantidad de números almacenados en la tabla de dígitos
  mov ecx, 8
  mov edx, 0                                 
  div ecx
  mov ecx, eax                                   ; Guardo la cantidad de numeros en la tabla
  mov edx, eax                                   ; Guardo una copia del resultado para volver
                                                 ; a cargarlo luego nuevamente a ecx

  ; Calculo la suma de los 4 bytes menos significativos
  pxor mm0, mm0                                  ; En mm0 acumulo la suma
  .loopSumaWords:
    pxor   mm1, mm1                              ; mm1 lo uso como buffer
    movq   mm1, [___tabla_de_digitos+(ecx-1)*8]
    paddb  mm0, mm1                              ; voy acumulando la suma en eax (suma en bytes)
  loop .loopSumaWords

  movq [___tasks_lineal_bss_st], mm0             ; Guardo el resultado

.fin_suma:
  hlt
jmp .fin_suma
