SECTION .main progbits
GLOBAL main

USE32
main:
  .kybrd_poll:
    xchg bx, bx             ; Magic breakpoint
    xor eax, eax
    in al, 0x64             ; Guarda en al lo que se lee del puerto 0x64
    bt eax, 0x00            ; Chequea el bit de paridad de status register del controlador del teclado (lo copia al CF)
    jnz .kybrd_poll         ; Salta a la etiqueta .kybrd_poll si hubo error de paridad (CF!=0)

    in al, 0x60             ; Guarda en al lo que se lee del puerto 0x60
    bt eax, 0x07            ; Copia el bit 0x07 del contenido de eax y lo copia en el CF (carry flag)
    jc .kybrd_poll          ; Jump short if carry (CF=1).
   
  jmp .kybrd_poll
  .guard:
    hlt
    jmp .guard

