SECTION  .kernel32 progbits
GLOBAL kernel32_init
EXTERN main
EXTERN CS_SEL_32

USE32
kernel32_init:
  xchg bx, bx                  ;Magic breakpoint
  jmp dword CS_SEL_32:main   
  .guard:
    hlt
    jmp .guard
