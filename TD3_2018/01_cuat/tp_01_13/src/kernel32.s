GLOBAL kernel32_init
GLOBAL INDICE_TABLA_DIGITOS

EXTERN main
EXTERN RAM_CS_SEL_32
EXTERN RAM_DS_SEL
EXTERN RAM_LDT_SEL
EXTERN TASK0_SEL_K_MODE
EXTERN TASK1_SEL_K_MODE
EXTERN TABLA_ISRs
EXTERN RAM_GDT
EXTERN rom_idtr
EXTERN ram_gdtr
EXTERN CONTADOR_RTC
EXTERN ___inicio_idt_ram
EXTERN ___tabla_de_digitos
EXTERN ___fin_tabla_de_digitos
EXTERN __L0_STACK_LINEAL_ADDR_END
EXTERN __L3_STACK_LINEAL_ADDR_END
EXTERN ___tasks_lineal_data_st
EXTERN ___tasks_lineal_addr_st
EXTERN ___task0_cr3
EXTERN ___task1_cr3
EXTERN ___task2_cr3
EXTERN __set_tasks_paging_tables
EXTERN ___task0_data_st

EXTERN inicioTarea1
EXTERN inicioTarea2
EXTERN __T1_L3_STACK_END
EXTERN __T2_L3_STACK_END

SECTION .kernel32 progbits
USE32
kernel32_init: 
  ; Inicialización del controlador de interrupciones ---------------------------------------------------------
    call initPIC

  ; Inicialización PIT----------------------------------------------------------------------------------------
    call initPIT

  ; Carga de TSSs en memoria----------------------------------------------------------------------------------
    ; Cargo TSS (única para definir los stacks de nivel 0 y nivel 3)
      push ___task0_data_st               ; Dirección física de comienzo de la TSS de la tarea
      push ___tasks_lineal_addr_st        ; Dirección lineal de comienzo del código de la tarea
      mov eax, ___task0_cr3               ; CR3 de la tarea
      and eax, 0xfffff000
      push eax                            
      push __L3_STACK_LINEAL_ADDR_END     ; Pila nivel3
      push __L3_STACK_LINEAL_ADDR_END     ; Pila nivel2
      push __L3_STACK_LINEAL_ADDR_END     ; Pila nivel1
      push __L0_STACK_LINEAL_ADDR_END     ; Pila nivel0
      call cargaTSS

  ; Carga GDT de RAM----------------------------------------------------------------------------------------------
    ; Agrego los descriptores de las tareas en la GDT
      push ___tasks_lineal_data_st        ; Dirección de inicio de la TSS
      push TASK0_SEL_K_MODE               ; Selector dentro de la GDT
      call cargaDescriptorDeTareaGDT
    ; Vuelvo a cargar gdtr para usar la GDT que está en RAM
      o32 lgdt  [cs:ram_gdtr]             ; Carga registro gdtr para usar la GDT de RAM
      jmp   .flush_prefetch_queue         ; Limpia el pipeline de ejecución (el jmp provoca el flush del pipeline)
      .flush_prefetch_queue:

  ; Generación de la tabla IDT------------------------------------------------------------------------------------
    ; Cargo los descriptores de las excepciones en la IDT
      push 20                             ; Número de descriptores a insertar en la IDT
      push TABLA_ISRs                     ; Dirección inicial de la tabla donde están las direcciones de las ISRs
      push 0x8f                           ; Atributos del descriptor: presente=1, dpl=0, s=0, tipo=trap gate
      push ___inicio_idt_ram              ; Índice de la IDT a partir del cual voy a insertar los descriptores
      call cargaIDT                       ; Carga descriptores IDT en RAM
    ; Cargo los descriptores de las interrupciones en la IDT
      push 2                              ; Número de descriptores a insertar en la IDT
      push TABLA_ISRs+(20*4)              ; Dirección inicial de la tabla donde están las direcciones de las ISRs
      push 0x8e                           ; Atributos del descriptor: presente=1, dpl=0, s=0, tipo=interrupt gate
      push ___inicio_idt_ram+(32*8)       ; Índice de la IDT a partir del cual voy a insertar los descriptores
      call cargaIDT                       ; Cargo la descriptores IDT en RAM
      lidt [rom_idtr]                     ; Carga registro idtr

    ; Cargo los descriptores de las interrupciones en la IDT 
    ; (NO SE POR QUE SE ME GENERA EN EL ARRANQUE ESTA INTERRUPCION, genero la entrada en la idt para catchearla)
      push 1                              ; Número de descriptores a insertar en la IDT
      push TABLA_ISRs+(22*4)              ; Dirección inicial de la tabla donde están las direcciones de las ISRs
      push 0x8e                           ; Atributos del descriptor: presente=1, dpl=0, s=0, tipo=interrupt gate
      push ___inicio_idt_ram+(39*8)       ; Índice de la IDT a partir del cual voy a insertar los descriptores
      call cargaIDT                       ; Cargo la descriptores IDT en RAM
      lidt [rom_idtr]                     ; Carga registro idtr

  ; Inicializo pilas nivel 0 de las tareas que se van a usar en la conmutación de tareas
    call initStacksL0

  ; Paginación-----------------------------------------------------------------------------------------------------
    ; Armo las tablas de paginación para la tarea0, tarea1 y tarea2
    call __set_tasks_paging_tables
    ; Cargo CR3
      mov eax, ___task0_cr3
      and eax, 0xfffff000
      mov cr3, eax                        ; Cargo CR3 con la dirección de inicio del directorio de tablas de página
    ; Habilito paginación
      mov eax, cr0
      or  eax, 0x80000000
      mov cr0, eax                        ; Habilito bit de paginación

  ; Jump a la tarea idle y carga de TR
  jmp TASK0_SEL_K_MODE:0
.guard:
  hlt
jmp .guard

;-----------------------;
;        initPIC        ;
;-----------------------;
initPIC:
  pushad
  ;Inicialización PICs
  mov bx, 0x2028      ; Base de los PICS (0x20 PIC1 0x28 PIC2)

  ;Inicialización PIC Nº1 (Master)
  mov al, 0x11        ; ICW1
  out 0x20,al         ; IRQs activas x flanco/cascada/ICW4 requerida
                      
  mov al, bh          ; ICW2
  out 0x21, al        ; El PIC Nº1 arranca en INT tipo (BH)
                      
  mov al, 0x4         ; ICW3
  out 21h, al         ; PIC1 Master, Slave ingresa Int.x IRQ2 (ver PDF pag. 90)
                      
  mov al, 0x1         ; ICW4
  out 0x21, al        ; Modo 8086/Non buffered mode/Not special fully nested mode
                      
  mov al, 0xfc        ; Habilito la INT1 (teclado) y la INT0 (rtc)
  out 0x21, al        

  ;Inicialización PIC Nº2 (Slave)
  mov al, 11h         ;ICW1
  out 0xA0, al        ;IRQs activas x flanco/cascada/ICW4 requerida
                      
  mov al, bl          ;ICW2
  out 0xA1, al        ;El PIC Nº2 arranca en INT tipo (BL)
                      
  mov al, 0x02        ;ICW3
  out 0xA1, al        ;PIC2 Slave, ingresa Int x IRQ2
                      
  mov al, 0x1         ;ICW4
  out 0xA1, al        ;Modo 8086
                      
  mov al, 0xff        ;Enmascaramos el resto de las Interrupciones 
  out 0xA1, al        ;(las del PIC Nº2)

  popad
ret

;------------------------;
;        initPIT         ;
;------------------------;
initPIT:
  pushad
  mov dx, 1193180/100   ; Valor de cuenta del contador 0 (frec. clock/frec. deseada (100hz->T=10mseg) )

  mov al, 110110b       ; Control word (cuenta binaria, modo señal cuadrada, envio LSB y luego MSB, contador0)
  out 0x43, al          ; Escritura del control word al control word register          

  mov ax, dx
  out 0x40, al          ; Envio cuenta LSB
  xchg ah, al
  out 0x40, al          ; Envio cuenta MSB
  popad
ret

;--------------------------------------------;
; Función para cargar descriptores en la IDT ;
;--------------------------------------------;
cargaIDT:
  push ebp                                  ; Resguardo ebp
  mov  ebp, esp                             ; Apunto ebp al final de la pila

  mov ecx, [ebp+20]                         ; Número de descriptores a insertar en la IDT
  mov ebx, [ebp+16]                         ; Dirección inicial de la tabla donde están las direcciones de las ISRs
  mov edx, [ebp+8]                          ; Índice de la IDT a partir del cual voy a insertar descriptores
  xor edi, edi
  
  .comienzoLoopCargaDescriptores:
    mov eax, [ebx+edi*4]                    ; Obtengo la dirección de la ISR (excepciones)
    mov byte [edx+edi*8]  , al              ; Offset ISR parte baja (little endian)
    mov byte [edx+edi*8+1], ah              ; Offset ISR parte baja (little endian)
    mov word [edx+edi*8+2], RAM_CS_SEL_32   ; Selector de segmento de código
    mov byte [edx+edi*8+4], 0               ; Selector de segmento de código
    mov eax, [ebp+12]                       ; Obtengo los atributos del descriptor
    mov byte [edx+edi*8+5], al              ; Cargo los atributos
    shr eax, 16                             ; Obtengo la parte alta del offset
    mov byte [edx+edi*8+6], al              ; Offset ISR parte alta (little endian)
    mov byte [edx+edi*8+7], ah              ; Offset ISR parte alta (little endian)
    inc edi                                 ; Incremento el índice
  loop .comienzoLoopCargaDescriptores

  pop ebp                                   ; Recupero ebp
  pop eax                                   ; Recupero la dirección de retorno
  pop ebx                                   ; Balanceo pila
  pop ebx                                   ; Balanceo pila
  pop ebx                                   ; Balanceo pila
  push eax                                  ; Vuelvo a meter la dirección de retorno en la pila
ret

;-----------------------------------------------------------------;
; Función para cargar un task gate en la IDT                      ;
;  Parámetros:                                                    ;
;    [ebp+12] Selector de TSS en la GDT                           ;
;    [ebp+8]  Dirección en donde se va a cargar la task gate      ;
;-----------------------------------------------------------------;
cargaTaskGateEnIDT:
  push ebp                                  ; Resguardo ebp
  mov  ebp, esp                             ; Apunto ebp al final de la pila
  
  ; Carga del descriptor del tipo task gate
   mov edi, [ebp+8]                         ; Guardo en edi la dirección en donde se va a cargar la task gate
   xor eax, eax
   mov [edi], ax                            ; word reservado
   mov eax, [ebp+12]
   mov [edi+2], ax                          ; cargo el selector del TSS que está en la GDT
   mov byte [edi+4], 0                      ; byte reservado
   mov byte [edi+5], 10000101b              ; P=1/DPL=00/S=0(descriptor de sistema)/Tipo:0101
   mov word [edi+6], 0                      ; word reservado

  pop ebp                                   ; Recupero ebp
  pop eax                                   ; Recupero la dirección de retorno
  pop ebx                                   ; Balanceo pila
  pop ebx                                   ; Balanceo pila
  push eax                                  ; Vuelvo a meter la dirección de retorno en la pila
ret

;------------------------------------------------------;
; Función para cargar un descriptor de tarea en la GDT ;
;------------------------------------------------------;
cargaDescriptorDeTareaGDT:
  push ebp                                  ; Resguardo ebp
  mov  ebp, esp                             ; Apunto ebp al final de la pila

  mov  ebx, [ebp+12]                        ; Dirección inicio TSS
  mov  edi, [ebp+8]                         ; Índice dentro de la GDT en donde se va a cargar el descriptor

  add edi, RAM_GDT                          ; Cargo en edx la dirección en donde se va a escribir el descriptor
  mov word [edi], 0x0068                    ; Cargo el campo límite bits 0-15
  mov [edi+2], bx                           ; Cargo el campo base bits 0-15

  shr ebx, 16
  mov byte [edi+4], bl                      ; Cargo el campo base bits 16-23

  mov byte [edi+5], 10001001b               ; P=1/DPL=00/0/TIPO=1001
  mov byte [edi+6], 00000000b               ; G=0(límiteX1)/0/0/AVL=0/Campo límite bits 16-19

  shr ebx, 8
  mov byte [edi+7], bl                      ; Cargo el campo base bits 16-23

  pop ebp                                   ; Recupero ebp
  pop eax                                   ; Recupero la dirección de retorno
  pop ebx                                   ; Balanceo pila
  pop ebx                                   ; Balanceo pila
  push eax                                  ; Vuelvo a meter la dirección de retorno en la pila
ret

;----------------------------------------------------------------------;
; Función para cargar una TSS en memoria                               ;
;  Parámetros:                                                         ;
;   [ebp+32] Dirección de comienzo de la TSS                           ;
;   [ebp+28] Valor inicial del eip de la tarea (Dirección de comienzo) ;
;   [ebp+24] Valor del registro CR3 de la tarea                        ;
;   [ebp+20] Dirección pila nivel3                                     ;
;   [ebp+16] Dirección pila nivel2                                     ;
;   [ebp+12] Dirección pila nivel1                                     ;
;   [ebp+8]  Dirección pila nivel0                                     ;
;----------------------------------------------------------------------;
cargaTSS:
  push ebp                           ; Resguardo ebp
  mov  ebp, esp                      ; Apunto ebp al final de la pila

  mov edi,  [ebp+32]                 ; Guardo en edi la dirección de inicio de la TSS
  mov dword [edi], 0                 ; Reserved + Previous Task Link

  mov eax, [ebp+8]
  mov dword [edi+4] , eax            ; Guardo dir. Pila nivel0
  mov dword [edi+8] , RAM_DS_SEL     ; Apunto el SS0 al segmento de datos

  mov eax, [ebp+12]        
  mov [edi+12], eax                  ; Guardo dir. Pila nivel1
  mov dword [edi+16], RAM_DS_SEL     ; Apunto el SS1 al segmento de datos

  mov eax, [ebp+16]        
  mov dword [edi+20], eax            ; Guardo dir. Pila nivel2
  mov dword [edi+24], RAM_DS_SEL     ; Apunto el SS2 al segmento de datos

  mov eax, [ebp+24]                  ; Guardo CR3
  mov [edi+28], eax
  
  mov eax, [ebp+28]                  ; Guardo eip
  mov [edi+32], eax

  mov dword [edi+36], 2              ; cargo con un valor valido EFLAGS
  mov dword [edi+40], 0              ; cargo con ceros eax
  mov dword [edi+44], 0              ; cargo con ceros ecx
  mov dword [edi+48], 0              ; cargo con ceros edx

  mov dword [edi+52], 0              ; cargo con ceros ebx
  mov eax, [ebp+20]
  mov dword [edi+56], eax            ; Guardo esp con la pila de nivel 3

  mov dword [edi+60], 0              ; cargo con ceros ebp
  mov dword [edi+64], 0              ; cargo con ceros esi
  mov word [edi+68] , 0              ; cargo con ceros edi

  mov word [edi+72] , RAM_DS_SEL     ; Cargo es con el selector del segmento de datos
  mov word [edi+76] , RAM_CS_SEL_32  ; Cargo cs con el selector del segmento de código
  mov word [edi+80] , RAM_DS_SEL     ; Cargo ss con el selector del segmento de datos
  mov word [edi+84] , RAM_DS_SEL     ; Cargo ds con el selector del segmento de datos
  mov word [edi+88] , RAM_DS_SEL     ; Cargo fs con el selector del segmento de datos
  mov word [edi+92] , RAM_DS_SEL     ; Cargo gs con el selector del segmento de datos
  mov word [edi+96] , RAM_LDT_SEL    ; Cargo el selector de segmento de la LDT
  mov word [edi+100], 0              ; Reserved
  mov word [edi+102], 0x68           ; Offset al I/O Map Base Address desde el inicio de la TSS
  mov byte [edi+104], 11111111b      ; Fin del I/O map

  pop ebp                            ; Recupero ebp
  pop eax                            ; Recupero la dirección de retorno
  pop ebx                            ; Balanceo pila
  pop ebx                            ; Balanceo pila
  pop ebx                            ; Balanceo pila
  pop ebx                            ; Balanceo pila
  pop ebx                            ; Balanceo pila
  pop ebx                            ; Balanceo pila
  pop ebx                            ; Balanceo pila
  push eax                           ; Vuelvo a meter la dirección de retorno en la pila
ret

;---------------------------------------------------------------------------------;
; Función initStacksL0                                                            ;
;   Inicializa los stacks de nivel 0 de las tareas para realizar la conmutación   ;
;---------------------------------------------------------------------------------;
initStacksL0:
  xor eax, eax
  ; Cargo el stack de nivel0 de la tarea1
  mov dword [__T1_L3_STACK_END-4] , 0x202                   ; Cargo con un valor válido EFLAGS
  mov dword [__T1_L3_STACK_END-8] , RAM_CS_SEL_32           ; CS
  mov dword [__T1_L3_STACK_END-12], ___tasks_lineal_addr_st ; EIP apunta al inicio de la tarea        
  mov [__T1_L3_STACK_END-16], eax                           ; registro de prop.gral. en 0
  mov [__T1_L3_STACK_END-20], eax                           ; registro de prop.gral. en 0
  mov [__T1_L3_STACK_END-24], eax                           ; registro de prop.gral. en 0
  mov [__T1_L3_STACK_END-28], eax                           ; registro de prop.gral. en 0
  mov dword [__T1_L3_STACK_END-32], 0x413ff3                ; ESP (va en ese valor)
  mov [__T1_L3_STACK_END-36], eax                           ; registro de prop.gral. en 0
  mov [__T1_L3_STACK_END-40], eax                           ; registro de prop.gral. en 0
  mov [__T1_L3_STACK_END-44], eax                           ; registro de prop.gral. en 0
  
  ; Cargo el stack de nivel0 de la tarea2
  mov dword [__T2_L3_STACK_END-4] , 0x202                   ; Cargo con un valor válido EFLAGS
  mov dword [__T2_L3_STACK_END-8] , RAM_CS_SEL_32           ; CS
  mov dword [__T2_L3_STACK_END-12], ___tasks_lineal_addr_st ; EIP apunta al inicio de la tarea
  mov [__T2_L3_STACK_END-16], eax                           ; registro de prop.gral. en 0
  mov [__T2_L3_STACK_END-20], eax                           ; registro de prop.gral. en 0
  mov [__T2_L3_STACK_END-24], eax                           ; registro de prop.gral. en 0
  mov [__T2_L3_STACK_END-28], eax                           ; registro de prop.gral. en 0
  mov dword [__T2_L3_STACK_END-32], 0x413ff3                ; ESP (va en ese valor)
  mov [__T2_L3_STACK_END-36], eax                           ; registro de prop.gral. en 0
  mov [__T2_L3_STACK_END-40], eax                           ; registro de prop.gral. en 0
  mov [__T2_L3_STACK_END-44], eax                           ; registro de prop.gral. en 0
ret
