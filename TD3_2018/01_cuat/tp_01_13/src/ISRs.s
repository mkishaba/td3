GLOBAL TABLA_ISRs
GLOBAL INDICE_TABLA_DIGITOS

EXTERN ___tabla_de_digitos
EXTERN ___tablas_de_paginacion
EXTERN TASK1_SEL_K_MODE
EXTERN __set_ptree_entry_32
EXTERN ___task0_cr3
EXTERN ___task1_cr3
EXTERN ___task2_cr3
EXTERN __L3_STACK_LINEAL_ADDR_END
EXTERN ___tasks_lineal_addr_st

SECTION .ISR progbits
;---------------------------------------------------;
; Rutinas de atención de interrupciones/excepciones ;
;---------------------------------------------------;
isr_0:
  xchg bx, bx            ; Magic breakpoint
  mov edx, 0
  .fin_isr0:
    hlt
  jmp .fin_isr0

isr_1:
  xchg bx, bx            ; Magic breakpoint
  mov edx, 1
  .fin_isr1:
    hlt
  jmp .fin_isr1

isr_2:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 2
  .fin_isr2:
    hlt
  jmp .fin_isr2

isr_3:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 3
  .fin_isr3:
    hlt
  jmp .fin_isr3

isr_4:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 4
  .fin_isr4:
    hlt
  jmp .fin_isr4

isr_5:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 5
  .fin_isr5:
    hlt
  jmp .fin_isr5

isr_6:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 6
  .fin_isr6:
    hlt
  jmp .fin_isr6

isr_7:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 7
  .fin_isr7:
    hlt
  jmp .fin_isr7

isr_8:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 8
  .fin_isr8:
    hlt
  jmp .fin_isr8

isr_9:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 9
  .fin_isr9:
    hlt
  jmp .fin_isr9

isr_10:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 10
  .fin_isr10:
    hlt
  jmp .fin_isr10

isr_11:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 11
  .fin_isr11:
    hlt
  jmp .fin_isr11

isr_12:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 12
  .fin_isr12:
    hlt
  jmp .fin_isr12

isr_13:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 13
  .fin_isr13:
    hlt
  jmp .fin_isr13

%ifdef PF_ENABLED                   ; Define para habilitar el handler de la #PF

; Excepción #PF Page Fault
PAGINA_DISPONIBLE:
  dd 0x10000000

isr_14:
  xchg bx,bx                        ; Magic breakpoint
  pop eax                           ; Saco de la pila el error code
  pushad

  and eax, 1
  cmp eax, 1                        ; Chequeo el bit de página no presente
  je .fin_isr14
    mov esi, [PAGINA_DISPONIBLE]
    mov eax, cr2                    ; Obtengo la dirección de memoria que generó la excepción
  
    push ebp
    mov  ebp, esp
    push 0x13                       ; Atributos de la página
    push eax                        ; Dirección lineal
    push esi                        ; Dirección física de la página a agregar
    push ___tablas_de_paginacion    ; Dirección de inicio del directorio de tablas de páginas
    call __set_ptree_entry_32
    leave
  
    add esi, 0x1000                 ; Actualizo la dirección de la próxima página disponible
    mov [PAGINA_DISPONIBLE], esi

  .fin_isr14:
  xchg bx, bx
  popad
iret

%else

  isr_14:
    xchg bx,bx             ; Magic breakpoint
    mov edx, 14
    .fin_isr14:
      hlt
  jmp .fin_isr14

%endif

isr_15:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 15
  .fin_isr15:
    hlt
  jmp .fin_isr15

isr_16:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 16
  .fin_isr16:
    hlt
  jmp .fin_isr16

isr_17:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 17
  .fin_isr17:
    hlt
  jmp .fin_isr17

isr_18:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 18
  .fin_isr18:
    hlt
  jmp .fin_isr18

isr_19:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 19
  iret
  .fin_isr19:
    hlt
  jmp .fin_isr19

;---------------------------------------------------------------------------------------------------;
;                                         ISR PIT                                                   ;
;---------------------------------------------------------------------------------------------------;

;--------------------------------------------------------------------------------; 
; Logica Scheduler                                                               ;
;   El loop completo del scheduler dura 200 mseg                                 ;
;   - La tarea1 se ejecuta cada 100 mseg (ventana de 20mseg para que se ejecute) ;
;   - La tarea2 se ejecuta cada 200 mseg (ventana de 20mseg para que se ejecute) ;
;--------------------------------------------------------------------------------;
;--------------------------------------------------------;
;Orden de ejecución de las tareas y tiempos de cada una: ;
;--------------------------------------------------------;
; Tarea | Tiempo switch tarea (mseg)    |
;  2    |   0                           |
;  0    |  20                           |
;  1    |  50                           |
;  0    |  70                           |
;  1    | 150                           |
;  0    | 170                           |
;  2    | 200 (vuelve a empezar el loop)|

ListaTareas:
  dd ___task2_cr3
  dd ___task0_cr3
  dd ___task1_cr3
  dd ___task0_cr3
  dd ___task1_cr3
  dd ___task0_cr3

; Los tiempos están expresados en decenas de mseg:
tiemposSwitchTareas:
  dd 0
  dd 2
  dd 5
  dd 7
  dd 15
  dd 17

CONTADOR_TMR:
  dd 0

INDICE_LISTA_TAREAS:
  dd 0

isr_32:
  pushad

  xor eax, eax
  xor esi, esi

  mov eax, [CONTADOR_TMR]
  mov esi, [INDICE_LISTA_TAREAS]

  cmp eax, [tiemposSwitchTareas+esi*4] ; Chequeo si se cumplió el tiempo para el cambio de tarea
  jne .fin_isr32
    ; Cambio la traducción de páginas (cambio de tarea)
    xor ecx, ecx
    mov ecx, [ListaTareas+esi*4]
    and ecx, 0xfffff000
    mov cr3, ecx                       ; Cargo el cr3 de la nueva tarea
    ; Apunto el EIP al inicio de la tarea
    mov dword [__L3_STACK_LINEAL_ADDR_END-12], ___tasks_lineal_addr_st
    inc esi
  jmp .fin_isr32
    
  .fin_isr32:
    inc eax
    cmp eax, 21                        ; Chequeo si el scheduler cumplio el ciclo completo
    jne .actualizaContador
    mov eax, 0                         ; Reseteo CONTADOR_TMR
    mov esi, 0                         ; Apunto el índice al inicio de la lista de tareas

    .actualizaContador:
    mov [CONTADOR_TMR], eax            ; Actualizo contador
    cmp esi, 6
    jne .actualizaIndice
    xor esi, esi                       ; Vuelvo el índice al inicio
    .actualizaIndice:
    mov [INDICE_LISTA_TAREAS], esi     ; Actualizo índice
    
    mov al, 0x20
    out 0x20, al                       ; Envío el end of interrupt al PIC
  popad
iret

;---------------------------------------------------------------------------------------------------;
;                                         ISR Teclado                                               ;
;---------------------------------------------------------------------------------------------------;
isr_33:
  pushad

  in al, 0x60                           ; Leemos el buffer de teclado

  mov bl, al
  and bl, 0x80                          ; Terminamos la isr si es un break code
  jnz .fin_isr33

  mov ecx, 17                           ; Cantidad de iteraciones para el loop que verifica si
                                        ; el caracter leído es hexadecimal

  cmp al, TECLA_ENTER                   ; Si se presiono la tecla "ENTER", copiamos el número de 64 bits a la tabla
  jne .loopVerificaCharHexValido
 
  .insertaNum64bitsEnTabla:
    ; Código de copiado del buffer de teclado a la tabla de dígitos

    ; Obtengo el índice del buffer de teclado que apunta a la última posición libre.
    ; Si el índice es 1 y la posición del digito es 1, significa que el buffer 
    ; está vacío -> no hago nada 
    xor eax, eax
    mov ecx, [INDICE_BUFFER_TECLADO]
    mov eax, [POSICION_DIGITO]
    cmp ecx, 1
    jne .bufferNoVacio
      cmp eax, 1
      je .fin_isr33

    .bufferNoVacio:

    cmp eax, 1
      je .procesaDigitos
    add ecx, 1                    ; Si la cantidad de digitos a insertar es impar
                                  ; hay que insertar un byte más 

    .procesaDigitos:
    ; Copio lo que está en el buffer de teclado a la tabla de dígitos
    mov edi, [INDICE_TABLA_DIGITOS]
    .copiaByteToTablaDigitos:
      mov eax, 0
      mov al, [BUFFER_TECLADO+ecx-1]
      mov [___tabla_de_digitos+edi], al
      inc edi 
    loop .copiaByteToTablaDigitos

    ; Si se ingresaron un numero impar de dígitos, tengo que shiftear lo
    ; el número resultante, 4 posiciones a la derecha
    xor eax, eax
    mov eax, [POSICION_DIGITO]
    cmp eax, 1
    je .noShiftDerecha
      mov edi, [INDICE_TABLA_DIGITOS]
      xor eax, eax
      xor ebx, ebx
      mov eax, [___tabla_de_digitos+edi]
      mov ebx, [___tabla_de_digitos+edi+4]

      ; Esto lo hago para shiftear el dígito menos significativo de los 4 bytes más significativos
      ; al dígito más significativo de los 4 bytes menos significativos
      shl ebx, 28
      shr eax, 4
      or  eax, ebx
      mov [___tabla_de_digitos+edi], eax    ; Guardo los 4 bytes menos significativos shifteados

      xor ebx, ebx
      mov ebx, [___tabla_de_digitos+edi+4]
      shr ebx, 4
      mov [___tabla_de_digitos+edi+4], ebx  ; Guardo los 4 bytes mas significativos shifteados
    .noShiftDerecha:

    ; Borro buffer de teclado
    mov eax, 0
    mov [BUFFER_TECLADO], eax             
    mov [BUFFER_TECLADO+4], eax
    mov [BUFFER_TECLADO+8], al

    ; Inicializo el índice de la posición del dígito
    mov byte [POSICION_DIGITO], 1

    ; Pongo a cero el índice del buffer de teclado
    mov [INDICE_BUFFER_TECLADO], eax

    ; Actualizo el indice de la tabla de dígitos
    mov edi, [INDICE_TABLA_DIGITOS]
    add edi, 8
    cmp edi, 65536                      ; Vuelve el índice a cero si se llego a los 64kb
    jne .actualizaIndiceTablaDigitos
    xor edi, edi

    .actualizaIndiceTablaDigitos:
      mov [INDICE_TABLA_DIGITOS], edi
      jmp .fin_isr33

  .loopVerificaCharHexValido:           ; Si el caracter leído es hexadecimal lo copiamos al buffer
    cmp al, [TABLA_CHAR_VALIDOS+ecx-1]
    je .agregaDigitoAlBuffer
    loop .loopVerificaCharHexValido
    jmp .fin_isr33

  .agregaDigitoAlBuffer:
    mov edi, [INDICE_BUFFER_TECLADO]
    mov al, [TABLA_CHAR_TRADUCIDOS+ecx-1]
    mov edx, [POSICION_DIGITO]
    cmp edx, 1
    je .guardaDigitoPos1

    .guardaDigitoPos0:
      mov byte [POSICION_DIGITO], 1    ; Inicializo el índice de la posición del dígito
      mov ebx, [BUFFER_TECLADO+edi]
      or  eax, ebx
      mov [BUFFER_TECLADO+edi], al

    ; Incrementamos el índice
    inc edi                              
    cmp edi, 9
    ; Si se llego al final del buffer, apunto el índice al principio
    jne .actualizaIndice
    mov edi, 1

  .actualizaIndice:
    mov [INDICE_BUFFER_TECLADO], edi
    jmp .fin_isr33

  .guardaDigitoPos1:
    shl al, 4
    mov [BUFFER_TECLADO+edi], al
    mov byte [POSICION_DIGITO], 0

  .fin_isr33:
    mov al, 0x20
    out 0x20, al                         ; Envío el end of interrupt al PIC
  popad
iret

;---------------------------------------------------------------------------;
; NO SÉ POR QUÉ SE GENERA ESTA INTERRUPCIÓN AL HABILITAR LAS INTERRUPCIONES ;
; AGREGO EL HANDLER PARA ATRAPARLA                                          ;
;---------------------------------------------------------------------------;
isr_39:
;  xchg bx, bx
  nop
iret

TECLA_ENTER equ 0x1c

ALIGN 16
TABLA_CHAR_VALIDOS:  ; Tabla que contiene los scan codes válidos a insertar
  db 0x0b            ; '0'
  db 0x02            ; '1'
  db 0x03            ; '2'
  db 0x04            ; '3'
  db 0x05            ; '4'
  db 0x06            ; '5'
  db 0x07            ; '6'
  db 0x08            ; '7'
  db 0x09            ; '8'
  db 0x0a            ; '9'
  db 0x1e            ; 'a'
  db 0x30            ; 'b'
  db 0x2e            ; 'c'
  db 0x20            ; 'd'
  db 0x12            ; 'e'
  db 0x21            ; 'f'

TABLA_CHAR_TRADUCIDOS:  ; Tabla que contiene los scan codes válidos a insertar
  db 0x00               ; '0'
  db 0x01               ; '1'
  db 0x02               ; '2'
  db 0x03               ; '3'
  db 0x04               ; '4'
  db 0x05               ; '5'
  db 0x06               ; '6'
  db 0x07               ; '7'
  db 0x08               ; '8'
  db 0x09               ; '9'
  db 0x0a               ; 'a'
  db 0x0b               ; 'b'
  db 0x0c               ; 'c'
  db 0x0d               ; 'd'
  db 0x0e               ; 'e'
  db 0x0f               ; 'f'

BUFFER_TECLADO:
  db 0
  db 0
  db 0
  db 0
  db 0
  db 0
  db 0
  db 0
  db 0
  
INDICE_TABLA_DIGITOS:
  dd 0

INDICE_BUFFER_TECLADO:
  dd 1

POSICION_DIGITO:
  db 1

TABLA_ISRs:          ; En esta tabla guardo las direcciones de las ISRs
  dd isr_0
  dd isr_1
  dd isr_2
  dd isr_3
  dd isr_4
  dd isr_5
  dd isr_6
  dd isr_7
  dd isr_8
  dd isr_9
  dd isr_10
  dd isr_11
  dd isr_12
  dd isr_13
  dd isr_14
  dd isr_15
  dd isr_16
  dd isr_17
  dd isr_18
  dd isr_19
  dd isr_32
  dd isr_33
  dd isr_39

