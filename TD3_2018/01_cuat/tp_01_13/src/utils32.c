/* 
  \file  init32.c
  \version 01.00
  \brief Definicion de funciones de inicializacion del procesador en 32b

  \author Christian Nigri <cnigri@utn.edu.ar>
  \date    18/04/2017
*/
#include "../inc/sys_types.h"
#include "../inc/defs.h"

/******************************************************************************
*
* @fn         __fast_memcpy
*
* @brief      Esta funcion copia length double words desde dst a src. 
*             No se raliza validacion de las regiones de memoria.
*             En caso de exito retorna EXITO o ERROR_DEFECTO en cualquier otra 
*             circunstancia
*
* @param [in] src puntero tipo double word. Especifica la direccion de origen.
*
* @param [in] dst puntero tipo double word. Especifica la direccion de destino.
*
* @param [in] length tipo double word. Especifica el numero de double wors a copiar.
*
* @return     tipo byte indicando si falla o no.
*
******************************************************************************/
__attribute__(( section(".init32"))) byte __fast_memcpy(const dword *src, dword *dst, dword length)
{

   byte status = ERROR_DEFECTO;

   if(length > 0)
   {
      
      while(length)
      {
         length--;
         *dst++ = *src++;
      }
      status = EXITO;   
   }

   return(status);
}

/******************************************************************************
*
* @fn         __set_ptree_entry_32
*
* @brief      Esta funcion inicializa las entradas del arbol de paginacion sin
*             PAE para la direccion lineal address_lin, asociada con la
*             direccion fisica address_phy, correspondientes a la base del arbol
*             de paginacion ptree_base, con los atributos page_attr.
*
* @param [in] ptree_base tipo double word. Especifica la direccion base del
*              arbol de paginacion.
*
* @param [in] addr_phy tipo double word. Especifica la direccion fisica de la
*             pagina.
*
* @param [in] addr_lin tipo double word. Especifica la direccion lineal de la
*             pagina.
*
* @param [in] page_attr tipo word. Especifica los atributos de las entradas
*             correspondientes a la nueva pagina.
*
* @return     tipo byte indicando si falla o no.
*
*******************************************************************************/
__attribute__(( section(".kernel32")))
void __set_ptree_entry_32( dword const ptree_base, dword addr_phy, dword addr_lin, dword page_attr) 
{
  dword *ptree_base_ptr = (dword *) ptree_base;
  // Cargo entrada en el directorio de tablas de página
  *(ptree_base_ptr + PDT_ENTRY) = ((ptree_base + (PDT_ENTRY+1) * 4096) & PAGE_ADDR_MASK) | PDT_ENTRY_ATTR; 
  // Cargo entrada en la tabla de páginas
  *((dword *)((byte *)ptree_base_ptr + (PDT_ENTRY+1) * 4096 + PAGE_TABLE_OFFSET * 4)) = (addr_phy & PAGE_ADDR_MASK) | page_attr;
}


/************************************************************************************
*  Función __set_paging_tables                                                      *
*  Genera tablas de paginación para identity mapping                                *
*                                                                                   *
*    Parámetros:                                                                    *
*      - dword ptree_base: dirección de inicio del directorio de tablas de páginas  *
*      - dword first_addr: primer dirección física a paginar en identity mapping    *
*      - dword max_addr: máxima dirección física a paginar en identity mapping      *
*                                                                                   *
*************************************************************************************/
__attribute__(( section(".kernel32")))
void __set_paging_tables( dword const ptree_base, dword first_addr, dword max_addr, dword attr )
{
  dword i,j;
  dword addr_phy=0;
  dword addr_lin=0;

  // Obtengo la primer entrada de la tabla de directorio
  dword firstDirectoryEntry = ((first_addr & PAGE_ADDR_MASK) / 4096) / 1024;
  // Obtengo la primer entrada de la tabla de páginas 
  dword firstPageTableEntry = ((first_addr & PAGE_ADDR_MASK) / 4096) % 1024;
  // Obtengo la última entrada de la tabla de directorio
  dword pageTableDirectoryEntries = ((max_addr & PAGE_ADDR_MASK) / 4096) / 1024;
  // Obtengo la última entrada de la última tabla de páginas
  dword remainEntries = ((max_addr & PAGE_ADDR_MASK) / 4096) % 1024;              

  if (pageTableDirectoryEntries>0) {
    // Cargo las primeras tablas de páginas, considerando la dirección física inicial
    // desde donde voy a empezar a paginar
    for (i=firstDirectoryEntry; i<firstDirectoryEntry+1; i++) {
      for (j=firstPageTableEntry; j<1024; j++) {
        addr_phy = i*4*1024*1024+j*4*1024;
        addr_lin = ((dword)i<<22)+((dword)j<<12);
        __set_ptree_entry_32(ptree_base, addr_phy, addr_lin, attr);
      }
    }
    // Cargo entradas para tablas de páginas completas (1024 entradas)
    for (i=firstDirectoryEntry+1; i<pageTableDirectoryEntries; i++) {
      for (j=0; j<1024; j++) {
        addr_phy = i*4*1024*1024+j*4*1024;
        addr_lin = ((dword)i<<22)+((dword)j<<12);
        __set_ptree_entry_32(ptree_base, addr_phy, addr_lin, attr);
      }
    }
    // Cargo las entradas restantes de la última tabla de páginas
    for (j=0; j<remainEntries; j++) {
      addr_phy = i*4*1024*1024+j*4*1024;
      addr_lin = ((dword)i<<22)+((dword)j<<12);
      __set_ptree_entry_32(ptree_base, addr_phy, addr_lin, attr);
    }
  } else {
    // Cargo las entradas restantes de la última tabla de páginas
    for (j=firstPageTableEntry; j<remainEntries; j++) {
      addr_phy = j*4*1024;
      addr_lin = (dword)j<<12;
      __set_ptree_entry_32(ptree_base, addr_phy, addr_lin, attr);
    }
  }
}

extern dword ___task0_cr3;
extern dword ___task1_cr3;
extern dword ___task2_cr3;
extern dword ___task0_vma_st;
extern dword ___task1_vma_st;
extern dword ___task2_vma_st;
extern dword ___task0_bss_st;
extern dword ___task1_bss_st;
extern dword ___task2_bss_st;
extern dword ___task0_data_st;
extern dword ___task1_data_st;
extern dword ___task2_data_st;
extern dword __T0_L0_STACK_END;
extern dword __T1_L0_STACK_END;
extern dword __T2_L0_STACK_END;
extern dword __T0_L3_STACK_END;
extern dword __T1_L3_STACK_END;
extern dword __T2_L3_STACK_END;
extern dword ___tasks_lineal_addr_st;
extern dword ___tasks_lineal_bss_st;
extern dword ___tasks_lineal_data_st;
extern dword __L3_STACK_LINEAL_ADDR_END;
extern dword __L0_STACK_LINEAL_ADDR_END;

//*************************************************************************************
//*  Función __set_tasks_paging_tables                                                *
//*  Genera las tablas de paginación para task0, task1 y task2                        *
//*************************************************************************************
__attribute__(( section(".kernel32")))
void __set_tasks_paging_tables(void) {
  // Armo tablas de Paginación en identity mapping (desde la dir. física 0 a 0x301000)
    __set_paging_tables( (dword) &___task0_cr3, 0, 0x00301000, 0x13);
    __set_paging_tables( (dword) &___task1_cr3, 0, 0x00301000, 0x13);
    __set_paging_tables( (dword) &___task2_cr3, 0, 0x00301000, 0x13);
  // Armo tablas de Paginación en identity mapping (desde la dir. física 310000 a 0x321000)
    __set_paging_tables( (dword) &___task0_cr3, 0x00310000, 0x00321000, 0x13);
    __set_paging_tables( (dword) &___task1_cr3, 0x00310000, 0x00321000, 0x13);
    __set_paging_tables( (dword) &___task2_cr3, 0x00310000, 0x00321000, 0x13);
  // Armo tablas de Paginación en identity mapping (desde la dir. física 0x3e0000 a 0x1fff8000) 
    __set_paging_tables( (dword) &___task0_cr3, 0x003e0000, 0x1fff8000, 0x13);
    __set_paging_tables( (dword) &___task1_cr3, 0x003e0000, 0x1fff8000, 0x13);
    __set_paging_tables( (dword) &___task2_cr3, 0x003e0000, 0x1fff8000, 0x13);
  // Armo tablas de Paginación en identity mapping (desde la dir. física 0x1fffb000 a 0x1fffc000) 
    __set_paging_tables( (dword) &___task0_cr3, 0x1fffb000, 0x1fffc000, 0x13);
    __set_paging_tables( (dword) &___task1_cr3, 0x1fffb000, 0x1fffc000, 0x13);
    __set_paging_tables( (dword) &___task2_cr3, 0x1fffb000, 0x1fffc000, 0x13);

  // Cargo las tablas de paginación específicas de cada tarea
    // sección .text
    __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &___task0_vma_st, (dword) &___tasks_lineal_addr_st, 0x13);
    __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &___task1_vma_st, (dword) &___tasks_lineal_addr_st, 0x13);
    __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &___task2_vma_st, (dword) &___tasks_lineal_addr_st, 0x13);
    // sección .bss
    __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &___task0_bss_st, (dword) &___tasks_lineal_bss_st, 0x13);
    __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &___task1_bss_st, (dword) &___tasks_lineal_bss_st, 0x13);
    __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &___task2_bss_st, (dword) &___tasks_lineal_bss_st, 0x13);
    // sección .data
    __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &___task0_data_st, (dword) &___tasks_lineal_data_st, 0x13);
    __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &___task1_data_st, (dword) &___tasks_lineal_data_st, 0x13);
    __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &___task2_data_st, (dword) &___tasks_lineal_data_st, 0x13);
    // pila nivel 0
    __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &__T0_L0_STACK_END, (dword) &__L0_STACK_LINEAL_ADDR_END, 0x13);
    __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &__T1_L0_STACK_END, (dword) &__L0_STACK_LINEAL_ADDR_END, 0x13);
    __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &__T2_L0_STACK_END, (dword) &__L0_STACK_LINEAL_ADDR_END, 0x13);
    // pila nivel 3
    __set_ptree_entry_32( (dword) &___task0_cr3, (dword) &__T0_L3_STACK_END, (dword) &__L3_STACK_LINEAL_ADDR_END, 0x13);
    __set_ptree_entry_32( (dword) &___task1_cr3, (dword) &__T1_L3_STACK_END, (dword) &__L3_STACK_LINEAL_ADDR_END, 0x13);
    __set_ptree_entry_32( (dword) &___task2_cr3, (dword) &__T2_L3_STACK_END, (dword) &__L3_STACK_LINEAL_ADDR_END, 0x13);
}
