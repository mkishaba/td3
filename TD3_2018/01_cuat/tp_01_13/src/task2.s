EXTERN TASK0_SEL_K_MODE
EXTERN INDICE_TABLA_DIGITOS
EXTERN ___tabla_de_digitos
EXTERN ___tasks_lineal_bss_st

SECTION .task2 progbits

;--------------------------------------------------------------------;
;                              Task 1                                ;
; Suma todos los números de 8 bytes guardados en la tabla de dígitos ;
;--------------------------------------------------------------------;
inicioTarea2:
  ; Obtengo el valor del índice de la tabla de dígitos 
  mov eax, [INDICE_TABLA_DIGITOS]
  cmp eax, 0
  je .fin_suma

  ; A partir del índice de la tabla de dígitos obtengo 
  ; la cantidad de números almacenados en la tabla de dígitos
  mov ecx, 8
  mov edx, 0                                 
  div ecx
  mov ecx, eax                                 ; Guardo la cantidad de numeros en la tabla
  mov edx, eax                                 ; Guardo una copia del resultado para volver
                                               ; a cargarlo luego nuevamente a ecx

  ; Calculo la suma de los 4 bytes menos significativos
  xor eax, eax                                 ; En eax acumulo la suma
  xor ebx, ebx                                 ; En ebx acumulo el carry
  .loopSumaLSBytes:
    add eax, [___tabla_de_digitos+(ecx-1)*8]   ; voy acumulando la suma en eax
    jnc .noAcumulaCarry
      add ebx, 1                               ; Si hubo carry, lo acumulo en ebx
    .noAcumulaCarry:
  loop .loopSumaLSBytes

  mov [___tasks_lineal_bss_st], eax            ; Guardo el resultado de los bytes menos significativos
  
  ; Calculo la suma de los 4 bytes más significativos 
  ; Si hay carry no los considero porque el número va a ser de 64 bits
  mov ecx, edx                                 ; Cantidad de iteraciones
  xor eax, eax                                 ; En eax acumulo la suma
  add eax, ebx                                 ; Le sumo el carry acumulado anteriormente
  .loopSumaMSBytes:
    add eax, [___tabla_de_digitos+(ecx-1)*8+4] ; voy acumulando la suma en eax
  loop .loopSumaMSBytes

  mov [___tasks_lineal_bss_st+4], eax          ; Guardo el resultado de los bytes más significativos

.fin_suma:
  hlt
jmp .fin_suma
