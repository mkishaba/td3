GLOBAL codigoTareaIdle

SECTION .task0 progbits

; Tarea idle
codigoTareaIdle:
  sti
.haltLoop:
  hlt
jmp .haltLoop
