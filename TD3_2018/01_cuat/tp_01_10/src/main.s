SECTION .main progbits
GLOBAL main

EXTERN INDICE_TABLA_DIGITOS

USE32
main:
  xchg bx, bx     ; Magic breakpoint
  sti             ; Habilitamos las interrupciones
  
  loop:
    nop
  jmp loop

  .fin:           ; Finalización del programa
    hlt
  jmp .fin
