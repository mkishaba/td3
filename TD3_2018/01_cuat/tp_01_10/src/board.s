USE16
SECTION  .start16 progbits

GLOBAL   late_board_init
EXTERN   __STACK_START_16
EXTERN   __STACK_END_16
GLOBAL   early_board_init
EXTERN   ..@early_board_init_return
GLOBAL   A20_Enable_No_Stack_return
EXTERN   A20_Enable_No_Stack
EXTERN   PIT_Set_Counter0
EXTERN   PIC_Config

early_board_init:
   xor   eax, eax
   mov   cr3, eax             ;Invalidar TLB

   jmp A20_Enable_No_Stack
   A20_Enable_No_Stack_return:

;   jmp ram_init
;   ram_init_return:

   mov   ax, cs                ; ¡¿por qué hace esto?!
   mov   ds, ax                ; ¡¿por qué hace esto?!
   mov   ax, __STACK_START_16
   mov   ss, ax
   mov   sp, __STACK_END_16

jmp ..@early_board_init_return


late_board_init:
   mov cx, 0x2                            ;Interrumpir cada 2mseg
   call PIT_Set_Counter0

   mov bx, 0x2820                         ;Base PIC0=0x20 PIC1=0x28
   call PIC_Config
ret
