%define ROM_SIZE          (64*1024)
%define ROM_START          0xf0000
%define RESET_VECTOR       0xffff0
%define STACK_MEM_SEGMENT  0x2000
%define STACK_MEM_OFFSET   0xffff

USE16                     ; Para que el compilador genere instrucciones de 16 bits
ORG ROM_START             ; Para decirle al compilador que calcule los saltos relativos a esta
                          ; direccion de memoria

start16:
   test eax, 0x0          ; Verificar que el uP no este en fallo
   jne ..@fault_end       ; En caso de error salta a la posicion ..@fault_end y pone el uP en hlt

   xor eax, eax
   mov cr3, eax           ; Invalidar TLB

 ; ---Inicializo la pila---
   mov ax, STACK_MEM_SEGMENT
   mov ss, ax
   mov ax, STACK_MEM_OFFSET
   mov sp, ax

 ; ---Habilito A20--- ;
   call enableA20

 ; ---Llamada a la funcion td3_memcopy-----------------
   push CODE_LENGTH       ; Cantidad de bytes a copiar
   push ROM_START/16      ; Origen parte alta
   push ROM_START         ; Origen parte baja
   push 0                 ; Destino parte alta
   push 0                 ; Destino parte baja
   call td3_memcopy

   jmp 0:ejecucionDesdeRAM  ; Paso a ejecutar el código desde RAM

ejecucionDesdeRAM:
 ; ---2da Llamada a la funcion td3_memcopy-----------------
   push CODE_LENGTH       ; Cantidad de bytes a copiar
   push 0                 ; Origen parte alta
   push 0                 ; Origen parte baja
   push 0x3000            ; Destino parte alta
   push 0                 ; Destino parte baja
   call td3_memcopy
   hlt

..@fault_end:
   hlt
   jmp ..@fault_end

; --------------------------------------------------------------------------- ;
; Funcion para copiar espacios de memoria                                     ;
; void td3_memcopy(void *destino, const void *origen, unsigned int num_bytes) ;
; --------------------------------------------------------------------------- ;
td3_memcopy:
   push ebp
   mov  ebp, esp      ; Para acceder a los argumentos pasados a la función
 
   ; -------------------------------------------------------------------- ;
   ; Cargo los registros correspondientes para hacer la copia de memoria  ;
   ; -------------------------------------------------------------------- ;
   mov cx,  [ebp+14]  ; Cantidad de bytes a copiar
   mov ds,  [ebp+12]  ; Cargo el origen de la copia (segmento)
   mov si,  [ebp+10]  ; Cargo el origen de la copia (offset)
   mov es,  [ebp+8]   ; Cargo el destino de la copia (segmento)
   mov di,  [ebp+6]   ; Cargo el destino de la copia (offset)
   cld                ; Para que los índices se incrementen en cada iteración (DF=0)
   repnz movsb        ; Ejecuto la copia de memoria

   xchg bx, bx        ; Magic breakpoint 
   pop ebp            ; Recupero ebp
   pop ax             ; Recupero direccion de retorno
   
   ; Balanceo la pila
   pop bx
   pop bx
   pop bx
   pop bx
   pop bx

   ; Vuelvo a meter en la pila la dirección de retorno
   push ax
   ret
td3_memcopy_fin:
; --------------------------------------------------------------------------------- ;
; --------------------------------------------------------------------------------- ;


; ------------------------------------------------------------------------------ ;
; Funcion para habilitar el bit A20 (para tener 32 bits de adddress)             ; 
; Hay que habilitar un bit del controlador de teclado                            ;
; ------------------------------------------------------------------------------ ;
; ----------------- ;
; Puertos del 8042  ;
; ----------------- ;
%define     PORT_A_8042    0x60        ;Puerto A de E/S del 8042
%define     CTRL_PORT_8042 0x64        ;Puerto de Estado del 8042
; ------------------ ;
; Comandos del 8042  ;
; ------------------ ;
%define     KEYB_DIS       0xAD        ;Deshabilita teclado con Command Byte
%define     KEYB_EN        0xAE        ;Habilita teclado con Command Byte
%define     READ_OUT_8042  0xD0        ;Copia en 0x60 el estado de OUT
%define     WRITE_OUT_8042 0xD1        ;Escribe en OUT lo almacenado en 0x60

enableA20:
   xor ax, ax

   ;Deshabilita el teclado
   mov al, KEYB_DIS
   out CTRL_PORT_8042, al

   ;Lee la salida
   mov al, READ_OUT_8042
   out CTRL_PORT_8042, al
   xor bx, bx
   in al, PORT_A_8042
   mov bx, ax

   ;Modifica el valor del A20
   mov al, WRITE_OUT_8042
   out CTRL_PORT_8042, al

   mov ax, bx
   or ax, 00000010b              ; Habilita el bit A20
   out PORT_A_8042, al

   ;Habilita el teclado
   mov al, KEYB_EN
   out CTRL_PORT_8042, al
   ret
; --------------------------------------------------------------------------------- ;
; --------------------------------------------------------------------------------- ;

dw 0xaa55                              ; Pongo una marca donde termina el código para debug

; Cálculo del largo en bytes de la porción de memoria
; que va de la etiqueta "start16" a este punto del codigo
CODE_LENGTH equ ($-start16)

; Rellena la memoria con "nop" para tener un binario de 64Kb y para que quede bien
; alineado el reset vector en la direccion 0xffff0
times (RESET_VECTOR - ROM_START - CODE_LENGTH) nop

;------------------------------------------------------------------------------------------
; RESET VECTOR (Esto es lo primero que deberia ejecutar el uP) (direccion 20bits: 0xffff0)
;------------------------------------------------------------------------------------------
reset_vector:             ; Este codigo deberia quedar en la posicion 0xffff0
  cli
  cld
  jmp 0xf000:start16
align 16
;---------------------------------------------------------------------------------
