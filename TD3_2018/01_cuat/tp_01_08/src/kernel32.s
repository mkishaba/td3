SECTION  .kernel32 progbits

GLOBAL kernel32_init
GLOBAL TABLA_ISRs
GLOBAL INDICE_TABLA_DIGITOS

EXTERN main
EXTERN CS_SEL_32
EXTERN IDTR
EXTERN CONTADOR_RTC
EXTERN ___tablas_de_sistema
EXTERN ___tabla_de_digitos
EXTERN ___fin_tabla_de_digitos
EXTERN ___tablas_de_sistema

USE32
kernel32_init: 

  ; Inicialización del controlador de interrupciones
  call initPIC

  ; Inicialización PIT(RTC)
  call initPIT

  ; Cargo los descriptores de las excepciones en la IDT
  push 20                           ; Número de descriptores a insertar en la IDT
  push TABLA_ISRs                   ; Dirección inicial de la tabla donde están las direcciones de las ISRs
  push ___tablas_de_sistema         ; Índice de la IDT a partir del cual voy a insertar los descriptores
  call cargaIDT                     ; Carga descriptores en RAM

  ; Cargo los descriptores de las interrupciones en la IDT
  push 2                            ; Número de descriptores a insertar en la IDT
  push TABLA_ISRs+(20*4)            ; Dirección inicial de la tabla donde están las direcciones de las ISRs
  push ___tablas_de_sistema+(32*8)  ; Índice de la IDT a partir del cual voy a insertar los descriptores
  call cargaIDT                     ; Cargo la IDT en RAM

  lidt [IDTR]                       ; Carga descriptores en RAM

  jmp dword CS_SEL_32:main          ; Jump al main
  .guard:
    hlt
    jmp .guard

;-----------------------;
;        initPIC        ;
;-----------------------;
initPIC:
  pushad
  ;Inicialización PICs
  mov bx, 0x2028      ; Base de los PICS (0x20 PIC1 0x28 PIC2)

  ;Inicialización PIC Nº1 (Master)
  mov al, 0x11        ; ICW1
  out 0x20,al         ; IRQs activas x flanco/cascada/ICW4 requerida
                      
  mov al, bh          ; ICW2
  out 0x21, al        ; El PIC Nº1 arranca en INT tipo (BH)
                      
  mov al, 0x4         ; ICW3
  out 21h, al         ; PIC1 Master, Slave ingresa Int.x IRQ2 (ver PDF pag. 90)
                      
  mov al, 0x1         ; ICW4
  out 0x21, al        ; Modo 8086/Non buffered mode/Not special fully nested mode
                      
  mov al, 0xfc        ; Habilito la INT1 (teclado) y la INT0 (rtc)
  out 0x21, al        

  ;Inicialización PIC Nº2 (Slave)
  mov al, 11h         ;ICW1
  out 0xA0, al        ;IRQs activas x flanco/cascada/ICW4 requerida
                      
  mov al, bl          ;ICW2
  out 0xA1, al        ;El PIC Nº2 arranca en INT tipo (BL)
                      
  mov al, 0x02        ;ICW3
  out 0xA1, al        ;PIC2 Slave, ingresa Int x IRQ2
                      
  mov al, 0x1         ;ICW4
  out 0xA1, al        ;Modo 8086
                      
  mov al, 0xff        ;Enmascaramos el resto de las Interrupciones 
  out 0xA1, al        ;(las del PIC Nº2)

  popad
ret

;------------------------;
;        init_RTC        ;
;------------------------;
initPIT:
  pushad
  mov dx, 1193180/10    ; Valor de cuenta del contador 0 (frec. clock/frec. deseada (10hz) )

  mov al, 110110b       ; Control word (cuenta binaria, modo señal cuadrada, envio LSB y luego MSB, contador0)
  out 0x43, al          ; Escritura del control word al control word register          

  mov ax, dx
  out 0x40, al          ; Envio cuenta LSB
  xchg ah, al
  out 0x40, al          ; Envio cuenta MSB
  popad
ret


;--------------------------------------------;
; Función para cargar descriptores en la IDT ;
;--------------------------------------------;
cargaIDT:
  push ebp                                  ; Resguardo ebp
  mov  ebp, esp                             ; Apunto ebp al final de la pila
  mov ecx, [ebp+16]                         ; Número de descriptores a insertar en la IDT
  mov ebx, [ebp+12]                         ; Dirección inicial de la tabla donde están las direcciones de las ISRs
  mov edx, [ebp+8]                          ; Índice de la IDT a partir del cual voy a insertar descriptores
  xor edi, edi
  
  .comienzoLoopCargaDescriptores:
    mov eax, [ebx+edi*4]                    ; Obtengo la dirección de la ISR (excepciones)
    mov byte [edx+edi*8]  , al              ; Offset ISR parte baja (little endian)
    mov byte [edx+edi*8+1], ah              ; Offset ISR parte baja (little endian)
    mov word [edx+edi*8+2], CS_SEL_32       ; Selector de segmento de código
    mov byte [edx+edi*8+4], 0               ; Selector de segmento de código
    mov byte [edx+edi*8+5], 0x8e            ; presente=1, dpl=0, s=0, tipo=interrupt gate
    shr eax, 16                             ; Obtengo la parte alta del offset
    mov byte [edx+edi*8+6], al              ; Offset ISR parte alta (little endian)
    mov byte [edx+edi*8+7], ah              ; Offset ISR parte alta (little endian)
    inc edi                                 ; Incremento el índice
  loop .comienzoLoopCargaDescriptores

  pop ebp                                   ; Recupero ebp
  pop eax                                   ; Recupero la dirección de retorno
  pop ebx                                   ; Balanceo pila
  pop ebx                                   ; Balanceo pila
  pop ebx                                   ; Balanceo pila
  push eax                                  ; Vuelvo a meter la dirección de retorno en la pila
ret

SECTION .ISR progbits
;---------------------------------------------------;
; Rutinas de atención de interrupciones/excepciones ;
;---------------------------------------------------;
isr_0:
  mov edx, 0
  .fin_isr0:
    hlt
  jmp .fin_isr0

isr_1:
  xchg bx, bx           ; Magic breakpoint
  mov edx, 1
  .fin_isr1:
    hlt
  jmp .fin_isr1

isr_2:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 2
  .fin_isr2:
    hlt
  jmp .fin_isr2

isr_3:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 3
  .fin_isr3:
    hlt
  jmp .fin_isr3

isr_4:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 4
  .fin_isr4:
    hlt
  jmp .fin_isr4

isr_5:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 5
  .fin_isr5:
    hlt
  jmp .fin_isr5

isr_6:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 6
  .fin_isr6:
    hlt
  jmp .fin_isr6

isr_7:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 7
  .fin_isr7:
    hlt
  jmp .fin_isr7

isr_8:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 8
  .fin_isr8:
    hlt
  jmp .fin_isr8

isr_9:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 9
  .fin_isr9:
    hlt
  jmp .fin_isr9

isr_10:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 10
  .fin_isr10:
    hlt
  jmp .fin_isr10

isr_11:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 11
  .fin_isr11:
    hlt
  jmp .fin_isr11

isr_12:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 12
  .fin_isr12:
    hlt
  jmp .fin_isr12

isr_13:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 13
  .fin_isr13:
    hlt
  jmp .fin_isr13

isr_14:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 14
  .fin_isr14:
    hlt
  jmp .fin_isr14

isr_15:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 15
  .fin_isr15:
    hlt
  jmp .fin_isr15

isr_16:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 16
  .fin_isr16:
    hlt
  jmp .fin_isr16

isr_17:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 17
  .fin_isr17:
    hlt
  jmp .fin_isr17

isr_18:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 18
  .fin_isr18:
    hlt
  jmp .fin_isr18

isr_19:
  xchg bx,bx             ; Magic breakpoint
  mov edx, 19
  iret
  .fin_isr19:
    hlt
  jmp .fin_isr19

;---------------------------------------------------------------------------------------------------;
;                                         ISR Timer                                                 ;
;---------------------------------------------------------------------------------------------------;
isr_32:                  ; ISR Timer
;  xchg bx,bx             ; Magic breakpoint
  pushad

  ; Leo e incremento el contador de interrupciones del timer0
  mov eax, [CONTADOR_RTC]
  inc eax
  mov [CONTADOR_RTC], eax

  mov al, 0x20
  out 0x20, al         ; Envío el end of interrupt al PIC

  popad
iret

;---------------------------------------------------------------------------------------------------;
;                                         ISR Teclado                                               ;
;---------------------------------------------------------------------------------------------------;
isr_33:
;  xchg bx,bx                            ; Magic breakpointa
  pushad

  in    al, 0x60                        ; Leemos el buffer de teclado

  mov bl, al
  and bl, 0x80                          ; Terminamos la isr si es un break code
  jnz .fin_isr33

  mov ecx, 16                           ; Cantidad de iteraciones para el loop que verifica si
                                        ; el caracter leído es hexadecimal

  cmp al, TECLA_ENTER                   ; Si se presiono la tecla "ENTER", copiamos el número de 64 bits a la tabla
  jne .loopVerificaCharHexValido
 
  .insertaNum64bitsEnTabla:
    ; Código de copiado del buffer de teclado a la tabla de dígitos
    mov edi, [INDICE_TABLA_DIGITOS]
    mov eax, [BUFFER_TECLADO]
    mov [___tabla_de_digitos+edi], eax    
    mov eax, [BUFFER_TECLADO+4]
    mov [___tabla_de_digitos+edi+4], eax

    ; Borro buffer de teclado
    mov eax, 0
    mov [BUFFER_TECLADO], eax             
    mov [BUFFER_TECLADO+4], eax
    mov [BUFFER_TECLADO+8], al

    ; Pongo a cero el índice del buffer de teclado
    mov [INDICE_BUFFER_TECLADO], eax

    ; Actualizo el indice de la tabla de dígitos
    add edi, 8
    cmp edi, 65536
    jne .actualizaIndiceTablaDigitos
    xor edi, edi

    .actualizaIndiceTablaDigitos:
      mov [INDICE_TABLA_DIGITOS], edi
      jmp .fin_isr33

  .loopVerificaCharHexValido:           ; Si el caracter leído es hexadecimal lo copiamos al buffer
    cmp al, [TABLA_CHAR_VALIDOS+ecx]
    je .agregaCharAlBuffer
    loop .loopVerificaCharHexValido
    jmp .fin_isr33

  .agregaCharAlBuffer:
    mov edi, [INDICE_BUFFER_TECLADO]
    mov al, [TABLA_CHAR_TRADUCIDOS+ecx]
    mov [BUFFER_TECLADO+edi], al
    inc edi                              ; Incrementamos el índice (overflow cuando llega a los 64K, buffer circular)
    cmp edi, 9
    jne .actualizaIndice
    mov edi, 0                           ; Si se llego al final del buffer, apunto el índice al principio

  .actualizaIndice:
    mov [INDICE_BUFFER_TECLADO], edi

  .fin_isr33:
    mov al, 0x20
    out 0x20, al                         ; Envío el end of interrupt al PIC
    popad
iret

TECLA_ENTER equ 0x1c

ALIGN 16
TABLA_CHAR_VALIDOS:  ; Tabla que contiene los scan codes válidos a insertar
  db 0x0b            ; '0'
  db 0x02            ; '1'
  db 0x03            ; '2'
  db 0x04            ; '3'
  db 0x05            ; '4'
  db 0x06            ; '5'
  db 0x07            ; '6'
  db 0x08            ; '7'
  db 0x09            ; '8'
  db 0x0a            ; '9'
  db 0x1e            ; 'a'
  db 0x30            ; 'b'
  db 0x2e            ; 'c'
  db 0x20            ; 'd'
  db 0x12            ; 'e'
  db 0x21            ; 'f'

TABLA_CHAR_TRADUCIDOS:  ; Tabla que contiene los scan codes válidos a insertar
  db 0x00               ; '0'
  db 0x01               ; '1'
  db 0x02               ; '2'
  db 0x03               ; '3'
  db 0x04               ; '4'
  db 0x05               ; '5'
  db 0x06               ; '6'
  db 0x07               ; '7'
  db 0x08               ; '8'
  db 0x09               ; '9'
  db 0x0a               ; 'a'
  db 0x0b               ; 'b'
  db 0x0c               ; 'c'
  db 0x0d               ; 'd'
  db 0x0e               ; 'e'
  db 0x0f               ; 'f'

BUFFER_TECLADO:
  db 0
  db 0
  db 0
  db 0
  db 0
  db 0
  db 0
  db 0
  db 0
  
INDICE_TABLA_DIGITOS:
  dd 0

INDICE_BUFFER_TECLADO:
  dd 0

TABLA_ISRs:          ; En esta tabla guardo las direcciones de las ISRs
  dd isr_0
  dd isr_1
  dd isr_2
  dd isr_3
  dd isr_4
  dd isr_5
  dd isr_6
  dd isr_7
  dd isr_8
  dd isr_9
  dd isr_10
  dd isr_11
  dd isr_12
  dd isr_13
  dd isr_14
  dd isr_15
  dd isr_16
  dd isr_17
  dd isr_18
  dd isr_19
  dd isr_32
  dd isr_33

