%include "./inc/processor-flags.h"

USE16
SECTION  .start16 progbits

GLOBAL start16
GLOBAL ..@early_board_init_return
EXTERN CS_SEL_32
EXTERN DS_SEL
EXTERN GDT_LENGTH
EXTERN rom_gdtr

EXTERN late_board_init
EXTERN early_board_init

start16:
   test  eax, 0x0                               ; Verificar que el uP no este en fallo
   jne   .fault_end

   xor   eax, eax
   mov   cr3, eax                               ; Invalidar TLB

   jmp   early_board_init                       ; Callout para agregar funciones de inicializacion
   ..@early_board_init_return:                  ; del procesador y controladores esenciales 

   ;->Deshabilitar cache<-
   mov   eax, cr0
   or    eax, (X86_CR0_NW | X86_CR0_CD)
   mov   cr0, eax
   wbinvd

   o32 lgdt  [cs:rom_gdtr]                      ; Carga GDT
   
   call late_board_init                         ; Callout para agregar funciones de inicializacion
                                                ; de los chipset de la placa. Inicializa el PIT y el PIC para este ejercicio

   ;->Establecer el up en MP<-
   smsw  ax                                     ; Store machine status word (guarda en ax el estado del registro CR0)
   or    ax, X86_CR0_PE                         ; Setea el bit de habilitación del Modo Protegido 
   lmsw  ax                                     ; Load machine status word (Cargo el valor de ax al registro CR0)

   jmp   .flush_prefetch_queue                  ; Limpia el pipeline de ejecución (el jmp provoca el flush del pipeline)
   .flush_prefetch_queue:

   o32 jmp dword CS_SEL_32:start32_launcher     ; jmp largo para cargar cs con el descriptor del segmento de código
                                                ; El o32 es para indicarle que la instrucción es para una máquina de 32 bits
                                                ; y dword para indicar que los registros son también de 32 bits
   .fault_end:
      hlt
      jmp .fault_end

SECTION  .start32 progbits
EXTERN __STACK_END_32
EXTERN __STACK_SIZE_32
EXTERN kernel32_init
EXTERN __fast_memcpy

EXTERN ___kernel_size
EXTERN ___kernel_vma_st
EXTERN ___kernel_lma_st

EXTERN ___main_size
EXTERN ___main_vma_st
EXTERN ___main_lma_st

EXTERN ___isr_size
EXTERN ___isr_vma_st
EXTERN ___isr_lma_st

EXTERN ___bss_size
EXTERN ___bss_vma_st
EXTERN ___bss_lma_st

USE32
start32_launcher:
   ;->Inicializar la pila   
   mov ax, DS_SEL
   mov ss, ax
   mov esp, __STACK_END_32
   ;->Inicializar la pila   
   xor ebx, ebx
   mov ecx, __STACK_SIZE_32
   .stack_init:
      push ebx
      loop .stack_init
   mov esp, __STACK_END_32

   ;->Inicializar la selectores datos
   mov ds, ax
   mov es, ax
   mov gs, ax
   mov fs, ax

   ;->Desempaquetar la ROM
   ;-->kernel
   push ebp
   mov ebp, esp
   push ___kernel_size
   push ___kernel_vma_st
   push ___kernel_lma_st
   call __fast_memcpy
   leave
   cmp eax, 1                   ; Analizo el valor de retorno (1 Exito -1 Fallo)
   jne .guard

   ;-->ISRs
   push ebp
   mov ebp, esp
   push ___isr_size
   push ___isr_vma_st
   push ___isr_lma_st
   call __fast_memcpy
   leave
   cmp eax, 1                   ; Analizo el valor de retorno (1 Exito -1 Fallo)
   jne .guard

   ;-->main
   push ebp
   mov ebp, esp
   push ___main_size
   push ___main_vma_st
   push ___main_lma_st
   call __fast_memcpy
   leave
   cmp eax, 1                   ; Analizo el valor de retorno (1 Exito -1 Fallo)
   jne .guard

   ;-->bss
   push ebp
   mov ebp, esp
   push ___bss_size
   push ___bss_vma_st
   push ___bss_lma_st
   call __fast_memcpy
   leave
   cmp eax, 1                   ; Analizo el valor de retorno (1 Exito -1 Fallo)
   jne .guard
 
   jmp CS_SEL_32:kernel32_init

   .guard:
      hlt
   jmp .guard
